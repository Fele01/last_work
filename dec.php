<?php

ini_set ( 'display_errors', 1 );
error_reporting ( E_ALL );

$handle = fopen('./response.txt', 'a+');

$encrypted = fgets($handle);

fclose($handle);

//$encrypted = "X7tFeLFbnmqIMhR39Wem2Ta7K7FFPKJpaP/V2bq17D2+oTLCXpCu8Fi/tX1L0xq8FbxA7njBC2xkKJpiEaAZfKp1wfqFhaUMnNLV6zGNNXQmWl9qnasxt3Rd+StNoHI2eraxJWxdKF0LQHu2sIJIWr/lIaBz3iv12Pvg4Hzmab9WCFhNwORbtAhTZuGsoIukjCU7vtcaBtxJngnJluSmWET1ev1Yu45XsA+Q2D8EeW2ObTa/RWi4wjfnH281ytYLM6TcKzZ17XDJXTpzW2DiuQZtm2+s7PjvU42pGV0Nr2ALo9zV0QCutISBW/pdDHcpwkopNO0x6/8GAQ+Llzx9GKiLOnDIODTp8PIopbp2r3BF4/Izzc4v5sY/B2UzEdY3d7x9c0HraQTkeyMB99eo8zH4zBGB+Imn2/Fy86r8juwQr+X/sk4cNGrifYQeX95lJYNClsnr5hE6/QsymI1U68NvTdpqcPVa4U8mtuLlkV05lpbGbFokUPGEzKVPKIbCK7RDNtMqz3G/pyE4HSsS0O5J2Ywy9N4oBclEc/uQKFna4AXc7Cc1lyZx9IyvRJhCmUjm696zfD+7FcR0fa7VXr9jL4pbHno2BvjJJW+wiXF2/NtNBzZCKNc23y6tpMKUGKjEcAI7WgYWn8l9YO2YEb++YNuhvLYvJjT1IA+GvmU=";

//echo $encrypted;

$encrypted = base64_decode($encrypted);
// Get the private Key

if (!$privateKey = openssl_pkey_get_private('file://./private.key'))
{
    die('Private Key failed');
}
$a_key = openssl_pkey_get_details($privateKey);
// Decrypt the data in the small chunks
$chunkSize = ceil($a_key['bits'] / 8);
$output = '';

while ($encrypted)
{
    $chunk = substr($encrypted, 0, $chunkSize);
    $encrypted = substr($encrypted, $chunkSize);
    $decrypted = '';
    if (!openssl_private_decrypt($chunk, $decrypted, $privateKey))
    {
        die('Failed to decrypt data');
    }
    $output .= $decrypted;
}
openssl_free_key($privateKey);

//echo $output;

$handle = fopen('./decResponse.txt', 'a+');

fwrite($handle, $output);

fclose($handle);