package APITests.CMS;

import APITests.CMS.UtilsCMS.MyResult;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import static com.jayway.restassured.RestAssured.given;

public class SubscriptionPlans extends BeforeAndAfterCMS {

    @Test(groups = {"validSP", "validUnsupportedRequestSP"}, priority = 0)
    public void subscriptionPlans() throws IOException {
        logger = extent.createTest("/api/v1/subscription/plans");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        Response resp = given().log().path().log().method()
                .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/plans");
        //String jsonAsString = resp.asString();   //take the response as string
        if(resp.getStatusLine().indexOf("200") != -1){
            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/subscriptionPlan.json");
            availableIds = returnsPlanIdsCMS(resp.asString());
            if (result == null) {
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR");
            } else {
                logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR");
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"unsupportedRequestTypeSP"}, priority = 0)
    public void subscriptionPlansPost() throws IOException {
        logger = extent.createTest("/api/v1/subscription/plans - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        Response resp = given().log().path().log().method()
                .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/plans");
        //String jsonAsString = resp.asString();   //take the response as string
        if (resp.getStatusLine().indexOf("200") != -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            //TRY TO CHECK FOR BODY
        } else if(resp.getStatusLine().indexOf("405") != -1) {
            logger.log(Status.PASS, "HTTP CODE 405");
            logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"unsupportedRequestTypeSP"}, priority = 0)
    public void subscriptionPlansPut() throws IOException {
        logger = extent.createTest("/api/v1/subscription/plans - PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        Response resp = given().log().path().log().method()
                .log().path().when().put("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/plans");
        //String jsonAsString = resp.asString();   //take the response as string
        if (resp.getStatusLine().indexOf("200") != -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            //TRY TO CHECK FOR BODY
        } else if(resp.getStatusLine().indexOf("405") != -1) {
            logger.log(Status.PASS, "HTTP CODE 405");
            logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"unsupportedRequestTypeSP"}, priority = 0)
    public void subscriptionPlansDelete() throws IOException {
        logger = extent.createTest("/api/v1/subscription/plans - DELETE");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        Response resp = given().log().path().log().method()
                .log().path().when().delete("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/plans");
        //String jsonAsString = resp.asString();   //take the response as string
        if (resp.getStatusLine().indexOf("200") != -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            //TRY TO CHECK FOR BODY
        } else if(resp.getStatusLine().indexOf("405") != -1) {
            logger.log(Status.PASS, "HTTP CODE 405");
            logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"validSP"}, priority = 1)
    public void subscriptionPlanClients() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{planId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        for(int i = 0; i < availableIds.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + availableIds.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/subscriptionClients.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR ID: " + availableIds.get(i));
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR ID: " + availableIds.get(i));
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            } else {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + " FOR ID: " + availableIds.get(i));
            }
        }
    }

    @Test(groups = {"validUnsupportedRequestSP"}, priority = 1)
    public void subscriptionPlanClientsPost() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{planId} - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        for(int i = 0; i < availableIds.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + availableIds.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"validUnsupportedRequestSP"}, priority = 1)
    public void subscriptionPlanClientsPut() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{planId} - PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        for(int i = 0; i < availableIds.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().put("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + availableIds.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"validUnsupportedRequestSP"}, priority = 1)
    public void subscriptionPlanClientsDelete() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{planId} - DELETE");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        for(int i = 0; i < availableIds.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().delete("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + availableIds.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidSP"}, priority = 2)
    public void subscriptionPlanClients2() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNonNumericPlanId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 8);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 2)
    public void subscriptionPlanClients2Post() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNonNumericPlanId} - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 8);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 2)
    public void subscriptionPlanClients2Put() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNonNumericPlanId} - PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 8);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().put("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 2)
    public void subscriptionPlanClients2Delete() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNonNumericPlanId} - DELETE");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 8);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().delete("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidSP"}, priority = 3)
    public void subscriptionPlanClients3() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 1) numeric invalid values with length [1, 20)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 5);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 3)
    public void subscriptionPlanClients3Post() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId} - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 1) numeric invalid values with length [1, 20)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 5);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 3)
    public void subscriptionPlanClients3Put() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId} - PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 1) numeric invalid values with length [1, 20)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 5);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().put("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 3)
    public void subscriptionPlanClients3Delete() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId} - DELETE");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 1) numeric invalid values with length [1, 20)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 5);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().delete("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidSP"}, priority = 4)
    public void subscriptionPlanClients4() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 2) numeric invalid values with length [20, +inf)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 20);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 500");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
            else if(resp.getStatusLine().indexOf("500") != -1){
                logger.log(Status.PASS, "INTENDED HTTP CODE 500");
                logger.log(Status.INFO, "RESPONSE RETURNED FOR: " + randomUsersGenerated.get(i));
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 500");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 4)
    public void subscriptionPlanClients4Post() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId} - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 2) numeric invalid values with length [20, +inf)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 20);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 4)
    public void subscriptionPlanClients4Put() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId} - PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 2) numeric invalid values with length [20, +inf)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 20);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().put("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"invalidUnsupportedRequestSP"}, priority = 4)
    public void subscriptionPlanClients4Delete() throws IOException {
        logger = extent.createTest("/api/v1/subscription/clients/{invalidNumericPlanId} - DELETE");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        //two situations 2) numeric invalid values with length [20, +inf)
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 20);    //generates 4 invalid user_agets
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().delete("http://cms.tim-alpha.gameloftstore.com/api/v1/subscription/clients/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "EXECUTED UNSUPPORTED REQUEST TYPE");
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

}