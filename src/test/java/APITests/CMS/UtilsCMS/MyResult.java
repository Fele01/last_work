package APITests.CMS.UtilsCMS;
import java.util.List;

public class MyResult {
    private final List<String> first;
    private final List<String> second;

    public MyResult(List<String> first, List<String> second) {
        this.first = first;
        this.second = second;
    }

    public List<String> getFirst() {
        return first;
    }

    public List<String> getSecond() {
        return second;
    }
}
