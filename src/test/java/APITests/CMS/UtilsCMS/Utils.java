package APITests.CMS.UtilsCMS;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.jayway.restassured.path.json.JsonPath.from;

public class Utils {

    public ArrayList<String> returnsPlanIdsCMS(String responseJSON) {
        JSONObject responseJObject = new JSONObject(responseJSON);
        ArrayList<String> arrayWithIds = new ArrayList<String>();
        JSONArray retrievedIds = responseJObject.getJSONArray("data");
        for(int i = 0; i < retrievedIds.length(); i++){
            JSONObject elementFromJsonArray = retrievedIds.getJSONObject(i);
            arrayWithIds.add(elementFromJsonArray.getString("id"));
        }
        System.out.println(arrayWithIds);
        return arrayWithIds;
    }

    public ArrayList<String> categoryIds(String responseJSON) {
        ArrayList<Map<String, ?>> responseAsArrayList = from(responseJSON).get("");
        ArrayList<String> arrayWithIds = new ArrayList<String>();
        for(int i = 0; i < responseAsArrayList.size(); i++){
            arrayWithIds.add(responseAsArrayList.get(i).get("id").toString());
        }
        System.out.println(arrayWithIds);
        return arrayWithIds;
    }

    public static void run(String comand) throws IOException, InterruptedException {
        Runtime p = Runtime.getRuntime();
        Process proc = p.exec(new String[]{"C:\\cygwin\\bin\\bash.exe", "-c", comand}, new String[]{"PATH=/cygdrive/c/cygwin/bin"});
        proc.waitFor();
        BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        while (br.ready())
            System.out.println(br.readLine());
    }

    public ArrayList<String> getClientIds(ArrayList<Map<String,?>> jsonAsArrayList){
        ArrayList<String> clientIdsTaken = new ArrayList<String>();
        for(int i = 0; i < jsonAsArrayList.size(); i++){
            if(jsonAsArrayList.get(i).get("client_id").toString().length() > 0){
                clientIdsTaken.add(jsonAsArrayList.get(i).get("client_id").toString());
            }
        }
        System.out.println(clientIdsTaken);
        return clientIdsTaken;
    }

    public ArrayList<String> getRandomValues(ArrayList<Map<String, ?>> jsonAsArrayList, int howManyToPass) {
        Random rand = new Random();
        int randomIndex;
        ArrayList<String> javaPlatforms = new ArrayList<String>();
        ArrayList<String> androidPlatforms = new ArrayList<String>();
        ArrayList<String> randomIdChosen = new ArrayList<String>();
        for (int i = 0; i < jsonAsArrayList.size(); i++) {
            if (jsonAsArrayList.get(i).get("agent").toString().indexOf("AndroidGeneric") == -1 && (jsonAsArrayList.get(i).get("agent").toString().indexOf("Android") != -1 || jsonAsArrayList.get(i).get("phone").toString().indexOf("Android") != -1)) {
                androidPlatforms.add(jsonAsArrayList.get(i).get("id").toString());
            } else if (jsonAsArrayList.get(i).get("agent").toString().indexOf("AndroidGeneric") == -1 && (jsonAsArrayList.get(i).get("phone").toString().indexOf("Android") == -1 && jsonAsArrayList.get(i).get("agent").toString().indexOf("Android") == -1)) {
                javaPlatforms.add(jsonAsArrayList.get(i).get("id").toString());
            }
        }
        for (int j = 0; j < howManyToPass; j++) {
            if (j % 2 == 0) {
                randomIndex = rand.nextInt(javaPlatforms.size());
                randomIdChosen.add(javaPlatforms.get(randomIndex));
            } else {
                randomIndex = rand.nextInt(androidPlatforms.size());
                randomIdChosen.add(androidPlatforms.get(randomIndex));
            }
        }
        randomIdChosen.add("6686");    //hardcoded cuz it's only one android generic
        System.out.println("DEBUG IDS: " + randomIdChosen);
        return randomIdChosen;
    }

    public ArrayList<String> getRandomUserAgents(ArrayList<Map<String, ?>> jsonAsArrayList, int howManyToPass) {
        Random rand = new Random();
        int randomIndex;
        ArrayList<String> allUserAgents = new ArrayList<String>();
        ArrayList<String> randomUserAgentChosen = new ArrayList<String>();
        for (int i = 0; i < jsonAsArrayList.size(); i++) {
            if (jsonAsArrayList.get(i).get("agent").toString().length() > 1 && jsonAsArrayList.get(i).get("agent").toString().indexOf("AndroidGeneric") == -1) {
                allUserAgents.add(jsonAsArrayList.get(i).get("agent").toString());
            }
        }
        System.out.println("check here");
        for (int j = 0; j < howManyToPass; j++) {
            randomIndex = rand.nextInt(allUserAgents.size());
            randomUserAgentChosen.add(allUserAgents.get(randomIndex));
        }
        System.out.println("DEBUG USERS: " + randomUserAgentChosen);
        return randomUserAgentChosen;
    }

    public ArrayList<String> generateNumeric(int howManyTimes, int lengthForStrings) {
        ArrayList<String> generatedRandomUsers = new ArrayList<String>();
        for(int j = 0; j < howManyTimes; j++) {
            String numbersGathered = "";
            char[] ch = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            Random random = new Random();
            for (int i = 0; i < lengthForStrings; i++) {
                numbersGathered += ch[random.nextInt(ch.length)];
            }
            if(Arrays.asList(generatedRandomUsers).contains(numbersGathered)){
                j = j - 1;
            }
            else{
                generatedRandomUsers.add(numbersGathered);
            }
        }
        System.out.println(generatedRandomUsers);
        return generatedRandomUsers;
    }

    public ArrayList<String> generateNumeric(int howManyTimes, int lengthForStrings, int biggerThenThis) {
        ArrayList<String> generatedRandomUsers = new ArrayList<String>();
        for(int j = 0; j < howManyTimes; j++) {
            String numbersGathered = "";
            char[] ch = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            Random random = new Random();
            for (int i = 0; i < lengthForStrings; i++) {
                numbersGathered += ch[random.nextInt(ch.length)];
            }
            if(Integer.parseInt(numbersGathered) < biggerThenThis){
                int numbersGatheredAsInt = biggerThenThis + Integer.parseInt(numbersGathered);
                numbersGathered = Integer.toString(numbersGatheredAsInt);
                if(Arrays.asList(generatedRandomUsers).contains(numbersGathered)){
                    j = j - 1;
                }
                else{
                    generatedRandomUsers.add(numbersGathered);
                }
            }
            else{
                if(Arrays.asList(generatedRandomUsers).contains(numbersGathered)){
                    j = j - 1;
                }
                else{
                    generatedRandomUsers.add(numbersGathered);
                }
            }
        }
        System.out.println(generatedRandomUsers);
        return generatedRandomUsers;
    }

    public ArrayList<String> generatingRandomString(int howManyTimes, int lengthForStrings) throws IOException {
        Random rand = new Random();
        int randomIndex;
        String generatedString;
        ArrayList<String> generatedRandomUsers = new ArrayList<String>();
        for (int i = 0; i < howManyTimes; i++) {
            randomIndex = rand.nextInt(lengthForStrings) + 1;
            byte[] array = new byte[randomIndex]; // length is bounded by 7
            new Random().nextBytes(array);
            generatedString = new String(array, Charset.forName("Unicode"));
            generatedRandomUsers.add(generatedString.replace("?", ""));
        }
//        final Formatter file = new Formatter("generatedStrings.txt");
//        file.format("%s", generatedRandomUsers.toString());
//        file.close();
        System.out.println(generatedRandomUsers);
        return generatedRandomUsers;
    }

    public void putTagForReport(String pathToTheReport) throws IOException {

        String content = new String(Files.readAllBytes(Paths.get("./test-output/" + pathToTheReport)), StandardCharsets.UTF_8);
        File file = new File("./test-output/" + pathToTheReport);
        if (content.indexOf("0</span> test(s) failed") != -1 && content.indexOf("10</span> test(s) failed") == -1) {
            File file2 = new File("./test-output/" + "[PASS]" + pathToTheReport);
            file.renameTo(file2);
        } else {
            File file2 = new File("./test-output/" + "[FAIL]" + pathToTheReport);
            file.renameTo(file2);
        }
    }

    private static boolean checkObjectIsEmpty(Object obj) {
        if (obj == null)
            return true;
        String objData = obj.toString();
        if (objData.length() == 0)
            return true;
        if (objData.equalsIgnoreCase("{}"))
            return true;
        return false;
    }

    public static String takeAndParseJSON(String pathToFileJSON, Boolean arrayJSON) throws ParseException, Exception {
        JSONParser parser = new JSONParser();
        String json;
        if (arrayJSON) {
            org.json.simple.JSONArray data = (org.json.simple.JSONArray) parser.parse(
                    new FileReader(pathToFileJSON));//path to the JSON file.
            json = data.toJSONString();
        } else {
            org.json.simple.JSONObject data = (org.json.simple.JSONObject) parser.parse(
                    new FileReader(pathToFileJSON));//path to the JSON file.
            json = data.toJSONString();
        }
        return json;
    }

    public static String generateCurrentTime() {
        String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(Calendar.getInstance().getTime());
        String currentTime = "_" + timeStamp;
        return currentTime;
    }

    public static MyResult compareListsOfKeys(ArrayList<String> keysModel, ArrayList<String> keysResponse) {
        Collection<String> listModel = new ArrayList(keysModel);    //the response should be as the model
        Collection<String> listResponse = new ArrayList(keysResponse);
        List<String> sourceList = new ArrayList<String>(listModel);
        List<String> destinationList = new ArrayList<String>(listResponse);
        System.out.println("DEBUG KEYS FOUND: " + listModel + " && " + listResponse);
        sourceList.removeAll(listResponse);    //what the model has and the response does not have
        destinationList.removeAll(listModel);    //what the response has and the model does not have
        System.out.println(sourceList + " && " + destinationList);
        if (sourceList.size() == 0 && destinationList.size() == 0) {
            return null;
        } else {
            return new MyResult(sourceList, destinationList);
        }
    }

    public static ArrayList<String> takeKeys(JSONObject objectJson) {
        ArrayList<String> foundKeys = new ArrayList<String>();
        Iterator iter = objectJson.keys();
        while (iter.hasNext()) {
            String key = (String) iter.next();
            foundKeys.add(key);
        }
        System.out.println("DEBUG: " + foundKeys);
        return foundKeys;
    }

    public static MyResult checkResponse(String responseJSON, String path) throws IOException {
        ArrayList<String> keysResponse, keysModel;
        String model = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
        JSONObject modelJSON = new JSONObject(model);
        System.out.println("\n---------------------\n");
        System.out.println(responseJSON.indexOf("["));
        System.out.println(responseJSON.indexOf("\"data\":"));
        System.out.println("\n---------------------\n");
        if (responseJSON.indexOf("[") == 0) {
            ArrayList<Map<String, ?>> responseAsArrayList = from(responseJSON).get("");
            for (int i = 0; i < responseAsArrayList.size() - 1; i++) {
                JSONObject responseJObjectOne = new JSONObject(responseAsArrayList.get(i));
                JSONObject responseJObjectTwo = new JSONObject(responseAsArrayList.get(i + 1));
                ArrayList<String> keysResponseOne = takeKeys(responseJObjectOne);
                ArrayList<String> keysResponseTwo = takeKeys(responseJObjectTwo);
                MyResult result = compareListsOfKeys(keysResponseOne, keysResponseTwo);
                if (result == null) {
                    continue;
                }
                else{
                    System.out.println(result.getFirst() + " && " + result.getSecond());
                    return new MyResult(result.getFirst(), result.getSecond());
                }
            }
            JSONObject responseJObject = new JSONObject(responseAsArrayList.get(0));
            keysResponse = takeKeys(responseJObject);
            keysModel = takeKeys(modelJSON);
            MyResult result = compareListsOfKeys(keysResponse, keysModel);
            if (result == null) {
                return null;
            } else {
                System.out.println("trying to return");
                return new MyResult(result.getFirst(), result.getSecond());
            }
        }
        else if(responseJSON.indexOf("\"data\":") != -1){
            ArrayList<Map<String, ?>> responseAsArrayList = from(responseJSON).get("data");
            for (int i = 0; i < responseAsArrayList.size() - 1; i++) {
                JSONObject responseJObjectOne = new JSONObject(responseAsArrayList.get(i));
                JSONObject responseJObjectTwo = new JSONObject(responseAsArrayList.get(i + 1));
                ArrayList<String> keysResponseOne = takeKeys(responseJObjectOne);
                ArrayList<String> keysResponseTwo = takeKeys(responseJObjectTwo);
                MyResult result = compareListsOfKeys(keysResponseOne, keysResponseTwo);
                if (result == null) {
                    continue;
                }
                else{
                    System.out.println(result.getFirst() + " && " + result.getSecond());
                    return new MyResult(result.getFirst(), result.getSecond());
                }
            }
            JSONObject responseJObject = new JSONObject(responseAsArrayList.get(0));
            keysResponse = takeKeys(responseJObject);
            keysModel = takeKeys(modelJSON);
            MyResult result = compareListsOfKeys(keysResponse, keysModel);
            if (result == null) {
                return null;
            } else {
                System.out.println("trying to return");
                return new MyResult(result.getFirst(), result.getSecond());
            }
        } else {
            JSONObject responseJObject = new JSONObject(responseJSON);
            keysResponse = takeKeys(responseJObject);
            keysModel = takeKeys(modelJSON);
            MyResult result = compareListsOfKeys(keysResponse, keysModel);
            if (result == null) {
                return null;
            } else {
                return new MyResult(result.getFirst(), result.getSecond());
            }
        }
    }
}