package APITests.CMS;

import APITests.CMS.UtilsCMS.MyResult;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;

public class Categories extends BeforeAndAfterCMS {

    @Test(groups = {"initial", "post", "postPut", "postDelete", "postPutDelete", "unsupportedReqWithValidValues"}, priority = 2)
    public void getAllCategories() throws IOException {
        logger = extent.createTest("/api/v1/category/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        Response resp = given().log().path().log().method()
                .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/category/");
        if(resp.getStatusLine().indexOf("200") != -1){
            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/categories.json");
            availableIds = categoryIds(resp.asString());
            if (result == null) {
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR");
            } else {
                logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR");
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"unsupportedRequestType"}, priority = 5)
    public void getAllCategories2() throws IOException {
        logger = extent.createTest("/api/v1/category/ - PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        Response resp = given().log().path().log().method()
                .log().path().when().put("http://cms.tim-alpha.gameloftstore.com/api/v1/category/");
        if (resp.getStatusLine().indexOf("200") != -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            //TRY TO CHECK FOR BODY
        } else if(resp.getStatusLine().indexOf("405") != -1) {
            logger.log(Status.PASS, "HTTP CODE 405");
            logger.log(Status.INFO, "EXECUTED  AN UNSUPPORTED REQUEST TYPE");
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"unsupportedRequestType"}, priority = 5)
    public void getAllCategories3() throws IOException {
        logger = extent.createTest("/api/v1/category/ - DELETE");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        Response resp = given().log().path().log().method()
                .log().path().when().delete("http://cms.tim-alpha.gameloftstore.com/api/v1/category/");
        if (resp.getStatusLine().indexOf("200") != -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            //TRY TO CHECK FOR BODY
        } else if(resp.getStatusLine().indexOf("405") != -1) {
            logger.log(Status.PASS, "HTTP CODE 405");
            logger.log(Status.INFO, "EXECUTED  AN UNSUPPORTED REQUEST TYPE");
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"initial", "post", "postPut", "postDelete", "postPutDelete"}, priority = 3)
    public void getSpecificCategory() throws IOException {
        logger = extent.createTest("/api/v1/category/{categoryId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        for(int i = 0; i < availableIds.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/category/" + availableIds.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/categories.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR");
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR");
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            } else {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                logger.log(Status.INFO, "CATEGORY ID USED:  " + availableIds.get(i));
            }
        }
    }

    @Test(groups = {"invalidIdAsString"}, priority = 5)
    public void getSpecificCategory2() throws IOException {
        logger = extent.createTest("/api/v1/category/{stringInvalidcategoryId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> invalidStrings =  generatingRandomString(10, 10);
        for(int i = 0; i < invalidStrings.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/category/" + invalidStrings.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/categories.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR");
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR");
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            } else {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                logger.log(Status.INFO, "CATEGORY ID USED:  " + invalidStrings.get(i));
            }
        }
    }

    @Test(groups = {"invalidIdAsNumeric"}, priority = 5)
    public void getSpecificCategory3() throws IOException {
        logger = extent.createTest("/api/v1/category/{stringInvalidcategoryId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> invalidStrings =  generateNumeric(10, 10);
        for(int i = 0; i < invalidStrings.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("http://cms.tim-alpha.gameloftstore.com/api/v1/category/" + invalidStrings.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/categories.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR");
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR");
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            } else {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                logger.log(Status.INFO, "CATEGORY ID USED:  " + invalidStrings.get(i));
            }
        }
    }

    @Test(groups = {"unsupportedRequestType"}, priority = 5)
    public void getSpecificCategory4() throws IOException {
        logger = extent.createTest("/api/v1/category/{stringInvalidcategoryId} - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> invalidStrings =  generatingRandomString(10, 10);
        for(int i = 0; i < invalidStrings.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/category/" + invalidStrings.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "CATEGORY ID USED:  " + invalidStrings.get(i));
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"unsupportedRequestType"}, priority = 5)
    public void getSpecificCategory5() throws IOException {
        logger = extent.createTest("/api/v1/category/{numericInvalidcategoryId} - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        ArrayList<String> invalidStrings =  generateNumeric(10, 10);
        for(int i = 0; i < invalidStrings.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/category/" + invalidStrings.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "CATEGORY ID USED:  " + invalidStrings.get(i));
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"unsupportedReqWithValidValues"}, priority = 5)
    public void getSpecificCategory6() throws IOException {
        logger = extent.createTest("/api/v1/category/{categoryId} - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        for(int i = 0; i < availableIds.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().post("http://cms.tim-alpha.gameloftstore.com/api/v1/category/" + availableIds.get(i));
            //String jsonAsString = resp.asString();   //take the response as string
            if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
                //TRY TO CHECK FOR BODY
            } else if(resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "HTTP CODE 405");
                logger.log(Status.INFO, "CATEGORY ID USED:  " + availableIds.get(i));
            }
            else{
                logger.log(Status.FAIL, "RESPONSE ISSUE SHOULD BE CODE 405");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"post", "postPut", "postDelete", "postPutDelete"}, priority = 0)
    public void createsCategory() throws IOException {
        logger = extent.createTest("/api/v1/category/ - POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        String postCategoryFile = new String(Files.readAllBytes(Paths.get("./src/test/java/APITests/CMS/postTestJsonData/" + "postTestData_06.json")));
//        long millis = System.currentTimeMillis();
//        postCategoryFile.replace("tim_test_", "tim_test_" + millis);
//        postCategoryFile.replace("Tim Test ", "Tim Test " + millis);
//        toPostJson = postCategoryFile;
        System.out.println(postCategoryFile);
        Response resp = given()
                .contentType(ContentType.JSON)
                .when()
                .body(postCategoryFile)
                .post("http://cms.tim-alpha.gameloftstore.com/api/v1/category/");
        if(resp.getStatusLine().indexOf("200") != -1) {
//            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/categories.json");
//            availableIds = categoryIds(resp.asString());
            logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
            logger.log(Status.INFO, "THE POST WAS SUCCESSFULLY EXECUTED");
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"postPut", "postPutDelete"}, priority = 1)
    public void updatesCategory() throws IOException {
        logger = extent.createTest("/api/v1/category/ - PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        String postCategoryFile = new String(Files.readAllBytes(Paths.get("./src/test/java/APITests/CMS/postTestJsonData/" + "postTestData_06.json")));
        postCategoryFile.replace("tim_test_11113", "tim_test_" + 11114);
        postCategoryFile.replace("Tim Test 11113", "Tim Test " + 11114);
        Response resp = given()
                .contentType(ContentType.JSON)
                .when()
                .body(postCategoryFile)
                .put("http://cms.tim-alpha.gameloftstore.com/api/v1/category/19617");
        if(resp.getStatusLine().indexOf("200") != -1) {
//            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/categories.json");
//            availableIds = categoryIds(resp.asString());
            logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
            logger.log(Status.INFO, "THE PUT WAS SUCCESSFULLY EXECUTED");
        }
        else{
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        }
    }

    @Test(groups = {"postDelete", "postPutDelete"}, priority = 4)
    public void deletesCategory() throws IOException {
        logger = extent.createTest("/api/v1/category/ - DELETE");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING CMS CATEGORY");
        for(int i = 0; i < availableIds.size(); i++) {
            Response resp = given()
                    .contentType(ContentType.JSON)
                    .when()
                    .delete("http://cms.tim-alpha.gameloftstore.com/api/v1/category/" + availableIds.size());
            if (resp.getStatusLine().indexOf("200") != -1) {
//            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/CMS/CMSModels/categories.json");
//            availableIds = categoryIds(resp.asString());
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "THE DELETE WAS SUCCESSFULLY EXECUTED FOR ID: " + availableIds.get(i));
            } else {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + " FOR ID: " + availableIds.get(i));
            }
        }
    }
}
