package APITests.Shop.GameCatalogueTests;


import Utils.Utils;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GameCatalogueBaseTest extends Utils {
    public String timeStarted = generateCurrentTime();
    public String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3ZWJ0b29scy10aW0tY21zIiwiaXNzIjoiR0FBIiwiaWF0IjoxNTMxOTA4MzM1fQ.vfTC9qH95XOwwF5iJlh77A6WkMiyiugv1aPhsp_Vm24DYIN0MXVHufB0SVosZD2hM_T5jHit-KZT3AhY_UqxoxD-PbtG0h-z8J5LRbGpqz7GBMi5mFp4Ow8CDF2FUmhZy3-EhBeAWzPqAgmoPsVv-_Nixbr8S4mVzxIZE-h1n1E";
    public String BASE_SHOP_ALPHA_URL = "http://ecomapis.gameloft.org/shop_alpha/catalogue/products";
    public String BASE_URL = "http://ecomapis.gameloft.org/catalogue/products";

    public String DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL = "http://ecomapis.gameloft.org/shop_alpha/catalogue/distributions/";
    public String DISTRIBUTED_CATALOGUE_SHOP_URL = "https://ecomapis.gameloft.org/catalogue/distributions/";

    public String CATEGORIES_URL = "http://ecomapis.gameloft.org/shop_alpha/catalogue/categories/";
    public String CATEGORIES_SHOP_URL = "https://ecomapis.gameloft.org/catalogue/categories/";
    public String GAMES_URL = "http://ecomapis.gameloft.org/shop_alpha/catalogue/games/";

    public String CATEGORIES_POST_JSON_PATH = "./src/test/java/APITests/GameCatalogueTests/PostTests/postTestJsonData/";
    public ExtentTest logger;
    public ExtentTest test;
    public ExtentReports extent = new ExtentReports();
    public String responseJSON;

    public File[] getListOfFiles(String directoryName) {
        ClassLoader classLoader = getClass().getClassLoader();
        File directory = new File(classLoader.getResource(directoryName).getFile());
        File[] files = directory.listFiles();
        System.out.println("Found " + files.length + " files in " + directoryName + " folder");
        return files;
    }

    public static String generateCurrentTime() {
        String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(Calendar.getInstance().getTime());
        String currentTime = "_" + timeStamp;
        return currentTime;
    }

    @BeforeTest
    public void setup(){
        ExtentHtmlReporter reporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/GameCatalogueAPIs" + timeStarted + ".html");
        reporter.loadXMLConfig("extent-config.xml");
        reporter.config().setTheme(Theme.DARK);
        extent.attachReporter(reporter);
        extent.setSystemInfo("HostName", "QA Automation");
        extent.setSystemInfo("Environment", "QA");
        extent.setSystemInfo("User Name", "Felecan Alexandru");
        //extent.loadConfig(new File(System.getProperty("user.dir") + "extent.config"));
        }

    @AfterMethod
    public void getResult(ITestResult result){
        if(result.getStatus() == ITestResult.FAILURE){
            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " Test case FAILED due to below issues", ExtentColor.RED));
            test.fail(result.getThrowable());
        }
    }

    @AfterTest
    public void endReport() throws IOException {
        extent.flush();
        putTagForReport("GameCatalogueAPIs" + timeStarted + ".html");
    }
}


