package APITests.Shop.GameCatalogueTests.PutTests;

import APITests.Shop.GameCatalogueTests.GameCatalogueBaseTest;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.jayway.restassured.RestAssured.given;

    public class GameCataloguePutTests extends GameCatalogueBaseTest {

        @Test(/*dataProvider = "JSONDataProviderCollection", */testName = "POST PRODUCTS", description = "POST A PRODUCT")
        public void PutGameCatalogueProducts_1() throws IOException {
        String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"putTestData_01.json")));
        Response postCategory = given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .when()
                .body(postCategoryFile)
                .log()
                .all()
                .put(CATEGORIES_URL)
                .then()
                .log()
                .all()
                .extract()
                .path("id");
        System.out.println(postCategory.asString());
        System.out.println(postCategory.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL);
        logger.getStatus();
        if (postCategory.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
        }
    }

        @Test(/*dataProvider = "JSONDataProviderCollection", */testName = "POST PRODUCTS", description = "POST A PRODUCT")
        public void PutGameCatalogueProducts_2() throws IOException {
            String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"putTestData_02.json")));
            Response postCategory = given()
                    .contentType(ContentType.JSON)
                    .header("Authorization", "Bearer " + token)
                    .when()
                    .body(postCategoryFile)
                    .log()
                    .all()
                    .put(CATEGORIES_URL)
                    .then()
                    .log()
                    .all()
                    .extract()
                    .path("id");
            System.out.println(postCategory.asString());
            System.out.println(postCategory.getStatusCode());
            logger = extent.createTest(CATEGORIES_URL);
            logger.getStatus();
            if (postCategory.getStatusLine().contains("200")) {
                logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            } else {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            }
        }

        @Test(/*dataProvider = "JSONDataProviderCollection", */testName = "POST PRODUCTS", description = "POST A PRODUCT")
        public void PutGameCatalogueProducts_3() throws IOException {
            String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"putTestData_03.json")));
            Response postCategory = given()
                    .contentType(ContentType.JSON)
                    .header("Authorization", "Bearer " + token)
                    .when()
                    .body(postCategoryFile)
                    .log()
                    .all()
                    .put(CATEGORIES_URL)
                    .then()
                    .log()
                    .all()
                    .extract()
                    .path("id");
            System.out.println(postCategory.asString());
            System.out.println(postCategory.getStatusCode());
            logger = extent.createTest(CATEGORIES_URL);
            logger.getStatus();
            if (postCategory.getStatusLine().contains("200")) {
                logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            } else {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            }
        }

        @Test(/*dataProvider = "JSONDataProviderCollection", */testName = "POST PRODUCTS", description = "POST A PRODUCT")
        public void PutGameCatalogueProducts_4() throws IOException {
            String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"putTestData_04.json")));
            Response postCategory = given()
                    .contentType(ContentType.JSON)
                    .header("Authorization", "Bearer " + token)
                    .when()
                    .body(postCategoryFile)
                    .log()
                    .all()
                    .put(CATEGORIES_URL)
                    .then()
                    .log()
                    .all()
                    .extract()
                    .path("id");
            System.out.println(postCategory.asString());
            System.out.println(postCategory.getStatusCode());
            logger = extent.createTest(CATEGORIES_URL);
            logger.getStatus();
            if (postCategory.getStatusLine().contains("200")) {
                logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            } else {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            }
        }

        @Test(/*dataProvider = "JSONDataProviderCollection", */testName = "POST PRODUCTS", description = "POST A PRODUCT")
        public void PutGameCatalogueProducts_5() throws IOException {
            String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"putTestData_05.json")));
            Response postCategory = given()
                    .contentType(ContentType.JSON)
                    .header("Authorization", "Bearer " + token)
                    .when()
                    .body(postCategoryFile)
                    .log()
                    .all()
                    .put(CATEGORIES_URL)
                    .then()
                    .log()
                    .all()
                    .extract()
                    .path("id");
            System.out.println(postCategory.asString());
            System.out.println(postCategory.getStatusCode());
            logger = extent.createTest(CATEGORIES_URL);
            logger.getStatus();
            if (postCategory.getStatusLine().contains("200")) {
                logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            } else {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
                logger.log(Status.INFO, " USED: " + postCategory.asString());
            }
        }
}
