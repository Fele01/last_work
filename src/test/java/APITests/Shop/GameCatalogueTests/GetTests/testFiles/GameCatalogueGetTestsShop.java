package APITests.Shop.GameCatalogueTests.GetTests.testFiles;

import APITests.Shop.PlatformsInfoTests.Utils.MyResult;
import APITests.Shop.GameCatalogueTests.GameCatalogueBaseTest;
import Utils.Utils;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;

public class GameCatalogueGetTestsShop extends GameCatalogueBaseTest {

    //PRODUCTS BY OWNED
    @Test(testName = "GETTING THE PRODUCTS", groups = {"SHOP"},description = "Using owned=0")
    public void GetGameCatalogueProducts_fromShop_1() throws IOException {
        int owned = 0;
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> ownedUsed = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().queryParam("owned", owned).when().get(BASE_URL+"/");
        System.out.println(resp.asString());
        if(resp.getStatusLine().indexOf("200") == -1){
            statusResponse.add(resp.getStatusLine());
            ownedUsed.add(Integer.toString(owned));
        }
        logger = extent.createTest(BASE_URL+"/?owned="+owned);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "OWNED USED: " + ownedUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        }
        else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "OWNED USED: " + owned + " - RESPONSE RETURNED:    " + resp.getStatusLine());

        }
    }

    @Test(testName = "GETTING THE PRODUCTS", groups = {"SHOP"}, description ="Using owned=1")
    public void GetGameCatalogueProducts_fromShop_2() throws IOException{
        int owned= 1;
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> ownedUsed = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().given().when().get(BASE_URL+"/?owned="+owned);
        System.out.println(resp.asString());
        if(resp.getStatusLine().indexOf("200") == -1){
            statusResponse.add(resp.getStatusLine());
            ownedUsed.add(Integer.toString(owned));
        }
        logger = extent.createTest(BASE_URL+"/?owned="+owned);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "OWNED USED: " + ownedUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        }
        else{
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "OWNED USED: " + owned + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS", groups = {"SHOP"}, description = "Using non integer parameter as owned")
    public void GetGameCatalogueProducts_fromShop_3() throws IOException{
        String owned = Utils.getRandomString(Utils.getRandomNumber(1, 45), true);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> ownedUsed = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().queryParam("owned", owned).when().get(BASE_URL+"/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if(resp.getStatusLine().indexOf("200") != -1){
            statusResponse.add(resp.getStatusLine());
            ownedUsed.add(owned);
        }
        logger = extent.createTest(BASE_URL+"/?owned="+owned);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "OWNED USED: " + ownedUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j) + " INSTEAD OF HTTP CODE 404");
            }
        }
        else{
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "OWNED USED: " + owned + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }



    //PRODUCTS BY ID
    @Test(testName = "GETTING THE PRODUCTS BY ID", groups = {"SHOP"},description = "Using productId=0")
    public void GetGameCatalogueProducts_fromShop_4() throws IOException {
        int productId = 0;
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().given().when().get(BASE_URL+"/"+productId);
        System.out.println(resp.asString());
        if(resp.getStatusLine().indexOf("404") == -1){
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(Integer.toString(productId));
        }
        logger = extent.createTest(BASE_URL+"/"+productId);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS BY ID");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        }
        else{
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS BY ID", groups = {"SHOP"}, description ="Using productId=1")
    public void GetGameCatalogueProducts_fromShop_5() throws IOException{
        int productId=1;
        ArrayList<String> statusResponse = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().given().when().get(BASE_URL+"/"+productId);
        System.out.println(resp.asString());
        logger = extent.createTest(BASE_URL+"/"+productId);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/productsGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }

        logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);


    }

    @Test(testName = "GETTING THE PRODUCTS BY ID", groups = {"SHOP"}, description ="Using productId= random number from 2 to 3856")
    public void GetGameCatalogueProducts_fromShop_6() throws IOException{
        int productId=Utils.getRandomNumber(2, 3856);
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().given().when().get(BASE_URL+"/"+productId);
        System.out.println(productId);
        System.out.println(resp.asString());
        logger = extent.createTest(BASE_URL+"/"+productId);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/productsGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS BY ID", groups = {"SHOP"}, description = "Using non integer parameter as productId")
    public void GetGameCatalogueProducts_fromShop_7() throws IOException{
        String productId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().when().get(BASE_URL+"/"+productId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if(resp.getStatusLine().indexOf("200") != -1){
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(productId);
        }
        logger = extent.createTest(BASE_URL+"/"+productId);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS BY ID");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        }
        else{
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }



    //DESCRIPTION BY ID
    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID", groups = {"SHOP"}, description = "Using productId=0")
    public void GetGameCatalogueProducts_fromShop_8() throws IOException{
        int productId = 0;
        ArrayList<String> statusResponse = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().when().get(BASE_URL +"/" + productId +"/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if(resp.getStatusLine().indexOf("200") != -1){
            statusResponse.add(resp.getStatusLine());
        }
        logger = extent.createTest(BASE_URL +"/" + productId +"/descriptions/");
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        }
        else{
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID", groups = {"SHOP"}, description = "Using productId=1")
    public void GetGameCatalogueProducts_fromShop_9() throws IOException{
        int productId = 1;
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().when().get(BASE_URL +"/" + productId +"/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_URL +"/" + productId +"/descriptions/");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP,  "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID", groups = {"SHOP"}, description = "Using productId= random number from 2 to 3856")
    public void GetGameCatalogueProducts_fromShop_10() throws IOException{
        int productId = Utils.getRandomNumber(2, 3856);
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().when().get(BASE_URL +"/" + productId +"/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("gameloft.org/shop/catalogue/"+ productId +"/descriptions/");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP,  "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION  BY ID", groups = {"SHOP"}, description = "Random string generated as a productId")
    public void GetGameCatalogueProducts_fromShop_11() throws IOException{
        String productId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().when().get(BASE_URL +"/" + productId +"/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if(resp.getStatusLine().indexOf("200") != -1){
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(productId);
        }
        logger = extent.createTest(BASE_URL +"/" + productId +"/descriptions/");
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        }
        else{
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }



    //DESCRIPTION BY LOCALE
    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", groups = {"SHOP"}, description = "Using productId=1807 and locale= en")
    public void GetGameCatalogueProducts_fromShop_12() throws IOException {
        int productId = 1807;
        String locale = "en";
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_URL + "/" + productId + "/descriptions/" + locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_URL + "/" + productId + "/descriptions/" + locale);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR LOCALE:" + locale);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR LOCALE: " + locale);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, "RESPONSE OK");
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", groups = {"SHOP"}, description = "sing productId= random number from 2 to 3856 and locale = random integer from 10 to 99 ")
    public void GetGameCatalogueProducts_fromShop_13() throws IOException {
        int productId = Utils.getRandomNumber(1, 3856);
        int locale = Utils.getRandomNumber(10, 99);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        ArrayList<String> localeUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_URL + "/" + productId + "/descriptions/" + locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if (resp.getStatusLine().indexOf("200") != -1) {
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(Integer.toString(productId));
            localeUsed.add(Integer.toString(locale));
        }
        logger = extent.createTest(BASE_URL +"/" + productId +"/descriptions/"+ locale);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION BY ID AND LOCALE");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
                logger.log(Status.INFO, "LOCALE USED: " + localeUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", groups = {"SHOP"}, description = "Using productId= random number from 2 to 3856  and locale = random generated 3 characters string")
    public void GetGameCatalogueProducts_fromShop_14() throws IOException{
        int productId = Utils.getRandomNumber(1, 3856);
        String locale = Utils.getRandomString(3,false);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        ArrayList<String> localeUsed = new ArrayList<>();
        Response resp =given().log().path().header("Authorization", "Bearer "+ token).log().method().when().get(BASE_URL +"/" + productId +"/descriptions/"+ locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if(resp.getStatusLine().indexOf("200") != -1){
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(Integer.toString(productId));
            localeUsed.add(locale);
        }
        logger = extent.createTest(BASE_URL +"/" + productId +"/descriptions/"+ locale);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION BY ID AND LOCALE");
        if(statusResponse.size() > 0){
            for(int j = 0; j < statusResponse.size(); j++){
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
                logger.log(Status.INFO, "LOCALE USED: " + localeUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        }
        else{
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " +resp.getStatusLine());
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", groups = {"SHOP"}, description = "Using productId= random number from 2 to 3856 and locale= random valid locale")
    public void GetGameCatalogueProducts_fromShop_15() throws IOException {
        int productId = Utils.getRandomNumber(1, 3856);
        String[] localeSet = {"fr", "en", "de", "es", "it", "br"};
        String locale = localeSet[Utils.getRandomNumber(0, localeSet.length - 1)];
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_URL + "/" + productId + "/descriptions/" + locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_URL + "/" + productId + "/descriptions/" + locale);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR LOCALE:" + locale);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR LOCALE: " + locale);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PLATFORM ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }



    //RELEASES BY PRODUCT ID
    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", groups = {"SHOP"}, description = "Using productId=0")
    public void GetGameCatalogueProducts_fromShop_16() throws IOException {
        int productId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get("http://ecomapis.gameloft.org/shop/catalogue/products/releases/index.php?product_id="+productId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("http://ecomapis.gameloft.org/shop/catalogue/products/releases/index.php?product_id="+productId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", groups = {"SHOP"}, description = "Using productId= random number form 1 to 3856")
    public void GetGameCatalogueProducts_fromShop_17() throws IOException {
        int productId = Utils.getRandomNumber(1, 3856);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get("http://ecomapis.gameloft.org/shop/catalogue/products/releases/index.php?product_id="+productId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("http://ecomapis.gameloft.org/shop/catalogue/products/releases/index.php?product_id="+productId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", groups = {"SHOP"}, description = "Using productId= random generated string")
    public void GetGameCatalogueProducts_fromShop_18() throws IOException {
        String productId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get("http://ecomapis.gameloft.org/shop/catalogue/products/releases/index.php?product_id="+productId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("http://ecomapis.gameloft.org/shop/catalogue/products/releases/index.php?product_id="+productId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }



    //RELEASES BY PRODUCT ID AND RELEASE ID
    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", groups = {"SHOP"}, description = "Using productId=0")
    public void GetGameCatalogueProducts_fromShop_19() throws IOException {
        int productId = 0;
        int releaseId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_URL+"/"+productId+"/releases/"+releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_URL+"/"+productId+"/releases/"+releaseId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", groups = {"SHOP"}, description = "Using productId= random number form 1 to 3856")
    public void GetGameCatalogueProducts_fromShop_20() throws IOException {
        int productId = Utils.getRandomNumber(1, 3856);
        int releaseId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_URL+"/"+productId+"/releases/"+releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_URL+"/"+productId+"/releases/"+releaseId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", groups = {"SHOP"}, description = "Using productId= random generated string and releaseId= random generated string")
    public void GetGameCatalogueProducts_fromShop_21() throws IOException {
        String productId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        String releaseId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_URL+"/"+productId+"/releases/"+releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_URL+"/"+productId+"/releases/"+releaseId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", groups = {"SHOP"}, description = "Using productId= random generated number from 1 to 3856 and releaseId= random generated string")
    public void GetGameCatalogueProducts_fromShop_22() throws IOException {
        int productId = Utils.getRandomNumber(1, 3856);
        String releaseId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_URL+"/"+productId+"/releases/"+releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_URL+"/"+productId+"/releases/"+releaseId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        }
    }



    //DISTRIBUTED CATALOGUE BY OPERATION ID
    @Test(testName = "GETTING THE PRODUCTS OPERATION BY OPERATION ID", groups = {"SHOP"}, description = "Using operationId=0")
    public void GetGameCatalogueProducts_fromShop_23() throws IOException {
        int operationId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/products");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/products");
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", groups = {"SHOP"}, description = "Using operationId= random number form 1 to 3856")
    public void GetGameCatalogueProducts_fromShop_24() throws IOException {
        int operationId = Utils.getRandomNumber(1, 3856);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/products");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/products");
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", groups = {"SHOP"}, description = "Using operationId= random generated string")
    public void GetGameCatalogueProducts_fromShop_25() throws IOException {
        String operationId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/products");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/products");
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "OPERATION ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    //DISTRIBUTED CATALOGUE BUILDS BY OPERATION ID
    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", groups = {"SHOP"}, description = "Using operationId=0")
    public void GetGameCatalogueProducts_fromShop_27() throws IOException {
        int operationId = 0;
        int platformId = 0;
        int languageIsoCode = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/builds/?platform="+platformId+"&language="+languageIsoCode);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
        } else  {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", groups = {"SHOP"}, description = "Using operationId= random number form 1 to 3856")
    public void GetGameCatalogueProducts_fromShop_28() throws IOException {
        int operationId = 0;
        int platformId = 0;
        int languageIsoCode = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/builds/?platform="+platformId+"&language="+languageIsoCode);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
        } else  {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", groups = {"SHOP"}, description = "Using operationId= random generated string")
    public void GetGameCatalogueProducts_fromShop_29() throws IOException {
        int operationId = 0;
        int platformId = 0;
        int languageIsoCode = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_SHOP_URL +operationId+"/builds/?platform="+platformId+"&language="+languageIsoCode);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
        } else  {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }



    //CATEGORIES
    @Test(testName = "GETTING THE CATEGORIES", groups = {"SHOP"}, description = "Using operationId=0")
    public void GetGameCatalogueProducts_fromShop_30() throws IOException {
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_SHOP_URL);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_SHOP_URL);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() );
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }



    //CATEGORIES BY ID
    @Test(testName = "CATEGORIES BY ID", groups = {"SHOP"}, description = "Using categoryId= random number form given list of numbers")
    public void GetGameCatalogueProducts_fromShop_31() throws IOException {
        int[] categoryIDs = {19611, 19612};
        int categoryId = categoryIDs[Utils.getRandomNumber(0,categoryIDs.length-1)];
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_SHOP_URL+categoryId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest( CATEGORIES_SHOP_URL+categoryId);
        logger.getStatus();
        if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
        } else if (!resp.getStatusLine().contains("200")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/categoryGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (!resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "CATEGORIES BY ID", groups = {"SHOP"}, description = "Using categoryId= random generated string")
    public void GetGameCatalogueProducts_fromShop_32() throws IOException {
        String categoryId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_SHOP_URL+categoryId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest( CATEGORIES_SHOP_URL+categoryId);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "CATEGORIES BY ID", groups = {"SHOP"}, description = "Using categoryId= random generated number")
    public void GetGameCatalogueProducts_fromShop_33() throws IOException {
        int categoryId = Utils.getRandomNumber(1, 20);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_SHOP_URL+categoryId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_SHOP_URL+categoryId);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId);
        } else  {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }



    //CATEGORIES BY ID AND PRODUCTS
    @Test(testName = "CATEGORIES BY ID", groups = {"SHOP"}, description = "Using categoryId= random number form given list of numbers")
    public void GetGameCatalogueProducts_fromShop_34() throws IOException {
        int[] categoryIDs = {19611, 19612};
        int categoryId = categoryIDs[Utils.getRandomNumber(0,categoryIDs.length-1)];
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_SHOP_URL+categoryId+"/products/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest( CATEGORIES_SHOP_URL+categoryId+"/products/");
        logger.getStatus();
        if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
        } else if (!resp.getStatusLine().contains("200")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/categoryGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (!resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "CATEGORIES BY ID", groups = {"SHOP"}, description = "Using categoryId= random generated string")
    public void GetGameCatalogueProducts_fromShop_35() throws IOException {
        String categoryId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_SHOP_URL+categoryId+"/products/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest( CATEGORIES_SHOP_URL+categoryId+"/products/");
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
//            } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "CATEGORIES BY ID", groups = {"SHOP"}, description = "Using categoryId= random generated number")
    public void GetGameCatalogueProducts_fromShop_36() throws IOException {
        int categoryId = Utils.getRandomNumber(1, 20);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_SHOP_URL+categoryId+"/products/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_SHOP_URL+categoryId+"/products/");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
        } else if (!resp.getStatusLine().contains("404")) {
//            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
//            if (result == null) {
//                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//                logger.log(Status.INFO, "OPERATION ID USED: " + productId);
//            } else {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
//                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
//                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());

        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }
}
