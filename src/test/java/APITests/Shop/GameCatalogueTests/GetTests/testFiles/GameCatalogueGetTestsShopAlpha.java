package APITests.Shop.GameCatalogueTests.GetTests.testFiles;
import APITests.Shop.PlatformsInfoTests.Utils.MyResult;
import APITests.Shop.GameCatalogueTests.GameCatalogueBaseTest;
import Utils.Utils;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import static com.jayway.restassured.RestAssured.given;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.ArrayList;


public class GameCatalogueGetTestsShopAlpha extends GameCatalogueBaseTest {


    //PRODUCTS BY OWNED
    @Test(testName = "GETTING THE PRODUCTS", description = "Using owned=0")
    public void GetGameCatalogueProducts_from_alphaShop_01() {
        int owned = 0;
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> ownedUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().queryParam("owned", owned).when().get(BASE_SHOP_ALPHA_URL + "/");
        System.out.println(resp.asString());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
            ownedUsed.add(Integer.toString(owned));
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/?owned=" + owned);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "OWNED USED: " + ownedUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
                //logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
            }
        } else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "OWNED USED: " + owned + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            //logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS", description = "Using owned=1")
    public void GetGameCatalogueProducts_from_alphaShop_02() {
        int owned = 1;
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> ownedUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().given().when().get(BASE_SHOP_ALPHA_URL + "/?owned=" + owned);
        System.out.println(resp.asString());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
            ownedUsed.add(Integer.toString(owned));
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/?owned=" + owned);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "OWNED USED: " + ownedUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "OWNED USED: " + owned + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS", description = "Using non integer parameter as owned")
    public void GetGameCatalogueProducts_from_alphaShop_03() {
        String owned = Utils.getRandomString(Utils.getRandomNumber(1, 45), true);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> ownedUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().queryParam("owned", owned).when().get(BASE_SHOP_ALPHA_URL + "/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
            ownedUsed.add(owned);
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/?owned=" + owned);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "OWNED USED: " + ownedUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j) + " INSTEAD OF HTTP CODE 404");
            }
        } else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "OWNED USED: " + owned + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }


    //PRODUCTS BY ID
    @Test(testName = "GETTING THE PRODUCTS BY ID", description = "Using productId=0")
    public void GetGameCatalogueProducts_from_alphaShop_04() {
        int productId = 0;
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().given().when().get(BASE_SHOP_ALPHA_URL + "/" + productId);
        System.out.println(resp.asString());
        if (resp.getStatusLine().contains("404")) {
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(Integer.toString(productId));
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS BY ID");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS BY ID", description = "Using productId=1")
    public void GetGameCatalogueProducts_from_alphaShop_05() throws IOException {
        int productId = 1;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().given().when().get(BASE_SHOP_ALPHA_URL + "/" + productId);
        System.out.println(resp.asString());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/productsGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS BY ID", description = "Using productId= random number from 2 to 3856")
    public void GetGameCatalogueProducts_from_alphaShop_06() throws IOException {
        int productId = Utils.getRandomNumber(2, 3856);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().given().when().get(BASE_SHOP_ALPHA_URL + "/" + productId);
        System.out.println(productId);
        System.out.println(resp.asString());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/productsGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS BY ID", description = "Using non integer parameter as productId")
    public void GetGameCatalogueProducts_from_alphaShop_07() {
        String productId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(productId);
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS BY ID");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }


    //DESCRIPTION BY ID
    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID", description = "Using productId=0")
    public void GetGameCatalogueProducts_from_alphaShop_08() {
        int productId = 0;
        ArrayList<String> statusResponse = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/");
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID", description = "Using productId=1")
    public void GetGameCatalogueProducts_from_alphaShop_09() throws IOException {
        int productId = 1;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("gameloft.org/shop_alpha/catalogue/" + productId + "/descriptions/");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID", description = "Using productId= random number from 2 to 3856")
    public void GetGameCatalogueProducts_from_alphaShop_10() throws IOException {
        int productId = Utils.getRandomNumber(2, 3856);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("gameloft.org/shop_alpha/catalogue/" + productId + "/descriptions/");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + productId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION  BY ID", description = "Random string generated as a productId")
    public void GetGameCatalogueProducts_from_alphaShop_11() {
        String productId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(productId);
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/");
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PRODUCT ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }


    //DESCRIPTION BY LOCALE
    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", description = "Using productId=1807 and locale= en")
    public void GetGameCatalogueProducts_from_alphaShop_12() throws IOException {
        int productId = 1807;
        String locale = "en";
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR LOCALE: " + locale);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", description = "Using productId= random number from 2 to 3856 and locale = random integer from 10 to 99 ")
    public void GetGameCatalogueProducts_from_alphaShop_13() {
        int productId = Utils.getRandomNumber(1, 3856);
        int locale = Utils.getRandomNumber(10, 99);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        ArrayList<String> localeUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(Integer.toString(productId));
            localeUsed.add(Integer.toString(locale));
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION BY ID AND LOCALE");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
                logger.log(Status.INFO, "LOCALE USED: " + localeUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }


    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", description = "Using productId= random number from 2 to 3856  and locale = random generated 3 characters string")
    public void GetGameCatalogueProducts_from_alphaShop_14() {
        int productId = Utils.getRandomNumber(1, 3856);
        String locale = Utils.getRandomString(3, false);
        ArrayList<String> statusResponse = new ArrayList<>();
        ArrayList<String> productIdUsed = new ArrayList<>();
        ArrayList<String> localeUsed = new ArrayList<>();
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        if (resp.getStatusLine().contains("200")) {
            statusResponse.add(resp.getStatusLine());
            productIdUsed.add(Integer.toString(productId));
            localeUsed.add(locale);
        }
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        logger.getStatus();
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS DESCRIPTION BY ID AND LOCALE");
        if (statusResponse.size() > 0) {
            for (int j = 0; j < statusResponse.size(); j++) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + productIdUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
                logger.log(Status.INFO, "LOCALE USED: " + localeUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
            }
        } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS DESCRIPTION BY ID AND LOCALE", description = "Using productId= random number from 2 to 3856 and locale= random valid locale")
    public void GetGameCatalogueProducts_from_alphaShop_15() throws IOException {
        int productId = Utils.getRandomNumber(1, 3856);
        String[] localeSet = {"fr", "en", "de", "es", "it", "br"};
        String locale = localeSet[Utils.getRandomNumber(0, localeSet.length - 1)];
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/descriptions/" + locale);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        } else if (!resp.getStatusLine().contains("404")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/descriptionGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
                logger.log(Status.INFO, "LOCALE USED: " + locale);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.FAIL, "RESPONSE AND MODEL ARE NOT SIMILAR FOR LOCALE: " + locale);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, "RESPONSE OK");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "LOCALE USED: " + locale);
        }
    }


    //RELEASES BY PRODUCT ID
    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", description = "Using productId=0")
    public void GetGameCatalogueProducts_from_alphaShop_16() {
        int productId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get("http://ecomapis.gameloft.org/shop_alpha/catalogue/products/" + productId + "/releases");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("http://ecomapis.gameloft.org/shop_alpha/catalogue/products/" + productId + "/releases");
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
        logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
    }


    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", description = "Using productId= random number form 1 to 3856")
    public void GetGameCatalogueProducts_from_alphaShop_17() {
        int productId = Utils.getRandomNumber(1, 3856);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get("http://ecomapis.gameloft.org/shop_alpha/catalogue/products/" + productId + "/releases");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("http://ecomapis.gameloft.org/shop_alpha/catalogue/products/" + productId + "/releases");
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY ID", description = "Using productId= random generated string")
    public void GetGameCatalogueProducts_from_alphaShop_18() {
        String productId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get("http://ecomapis.gameloft.org/shop_alpha/catalogue/products/" + productId + "/releases");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest("http://ecomapis.gameloft.org/shop_alpha/catalogue/products/" + productId + "/releases");
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }


    //RELEASES BY PRODUCT ID AND RELEASE ID
    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", description = "Using productId=0")
    public void GetGameCatalogueProducts_from_alphaShop_19() {
        int productId = 0;
        int releaseId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", description = "Using productId= random number form 1 to 3856 and releaseId=0")
    public void GetGameCatalogueProducts_from_alphaShop_20() {
        int productId = Utils.getRandomNumber(1, 3856);
        int releaseId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", description = "Using productId= random generated string and releaseId= random generated string")
    public void GetGameCatalogueProducts_from_alphaShop_21() {
        String productId = Utils.getRandomString(Utils.getRandomNumber(2, 20), true);
        String releaseId = Utils.getRandomString(Utils.getRandomNumber(2, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);

        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", description = "Using productId= random generated number from 1 to 3856 and releaseId= random generated string")
    public void GetGameCatalogueProducts_from_alphaShop_22() {
        int productId = Utils.getRandomNumber(1, 3856);
        String releaseId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        }
    }

    @Test(testName = "GETTING THE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID", description = "Using productId= random generated number from 1 to 3856 and releaseId= valid releaseId")
    public void GetGameCatalogueProducts_from_alphaShop_23() {
        int productId = Utils.getRandomNumber(1, 3856);
        int releaseId = Utils.getRandomNumber(1, 20);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(BASE_SHOP_ALPHA_URL + "/" + productId + "/releases/" + releaseId);
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID");
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId);
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "PRODUCT ID USED: " + productId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "RELEASE ID USED: " + releaseId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        }
    }


    //DISTRIBUTED CATALOGUE PRODUCTS BY OPERATION ID
    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", description = "Using operationId=0")
    public void GetGameCatalogueProducts_from_alphaShop_24() {
        int operationId = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/products");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/products");
        logger.getStatus();
        logger.assignCategory("API TESTING GAME CATALOGUE PRODUCTS RELEASE BY PRODUCT ID AND RELEASE ID");
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        }
    }

    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", description = "Using operationId= random number form 1 to 3856")
    public void GetGameCatalogueProducts_from_alphaShop_25() {
        int operationId = 15699;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/products/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/products");
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());

        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        }
    }

    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", description = "Using operationId= random generated string")
    public void GetGameCatalogueProducts_from_alphaShop_26() {
        String operationId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/products");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/products");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());

        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
        }
    }


    //DISTRIBUTED CATALOGUE BUILDS BY OPERATION ID
    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", description = "Using operationId=0")
    public void GetGameCatalogueProducts_from_alphaShop_27() {
        int operationId = 0;
        int platformId = 0;
        int languageIsoCode = 0;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/builds/?platform=" + platformId + "&language=" + languageIsoCode);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(GAMES_URL + operationId + "/" + platformId + "/" + languageIsoCode);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", description = "Using operationId= random number form 1 to 3856")
    public void GetGameCatalogueProducts_from_alphaShop_28() {
        int operationId = 0;
        int platformId = 1807;
        int languageIsoCode = 1;
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/builds/?platform=" + platformId + "&language=" + languageIsoCode);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(GAMES_URL + operationId + "/" + platformId + "/" + languageIsoCode);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", description = "Using operationId= random generated string")
    public void GetGameCatalogueProducts_from_alphaShop_29() {
        int operationId = 15699;
        int platformId = 1807;
        String languageIsoCode = "it";
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(DISTRIBUTED_CATALOGUE_ALPHA_SHOP_URL + operationId + "/builds/?platform=" + platformId + "&language=" + languageIsoCode);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(GAMES_URL + operationId + "/" + platformId + "/" + languageIsoCode);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }


    //CATEGORIES
    @Test(testName = "GETTING THE CATEGORIES", description = "Getting the categories API")
    public void GetGameCatalogueProducts_from_alphaShop_30() {
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_URL);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL);
        logger.getStatus();
        if (resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }


    //CATEGORIES BY ID
    @Test(testName = "CATEGORIES BY ID", description = "Using categoryId= random number form given list of numbers")
    public void GetGameCatalogueProducts_from_alphaShop_31() throws IOException {
        int[] categoryIDs = {19611, 19612};
        int categoryId = categoryIDs[Utils.getRandomNumber(0, categoryIDs.length - 1)];
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_URL + categoryId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL + categoryId);
        logger.getStatus();
        if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
        } else if (!resp.getStatusLine().contains("200")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/categoryGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (!resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "CATEGORIES BY ID", description = "Using categoryId= random generated string")
    public void GetGameCatalogueProducts_from_alphaShop_32() {
        String categoryId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_URL + categoryId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL + categoryId);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "CATEGORIES BY ID", description = "Using categoryId= random generated number")
    public void GetGameCatalogueProducts_from_alphaShop_33() {
        int categoryId = Utils.getRandomNumber(1, 20);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_URL + categoryId);
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL + categoryId);
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }


    //CATEGORIES BY ID AND PRODUCTS
    @Test(testName = "CATEGORIES BY ID", description = "Using categoryId= random number form given list of numbers")
    public void GetGameCatalogueProducts_from_alphaShop_34() throws IOException {
        int[] categoryIDs = {19612};
        int categoryId = categoryIDs[Utils.getRandomNumber(0, categoryIDs.length - 1)];
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_URL + categoryId + "/products/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL + categoryId + "/products/");
        logger.getStatus();
        if (resp.getStatusLine().contains("404")) {
            logger.log(Status.SKIP, " - RESPONSE RETURNED:    " + resp.getStatusLine() + " NO DESCRIPTION FOUND");
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
        } else if (!resp.getStatusLine().contains("200")) {
            MyResult result = Utils.checkResponse(resp.asString(), "./src/test/java/APITests/Shop/GameCatalogueTests/GetTests/toCompareJsonFiles/categoryGetModel.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId);
            } else {
                logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else if (!resp.getStatusLine().contains("404")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORY ID USED: " + categoryId + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(testName = "CATEGORIES BY ID", description = "Using categoryId= random generated string")
    public void GetGameCatalogueProducts_from_alphaShop_35()  {
        String categoryId = Utils.getRandomString(Utils.getRandomNumber(1, 20), true);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_URL + categoryId + "/products/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL + categoryId + "/products/");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId);
        }
    }

    @Test(testName = "CATEGORIES BY ID", description = "Using categoryId= random generated number")
    public void GetGameCatalogueProducts_from_alphaShop_36() {
        int categoryId = Utils.getRandomNumber(1, 20);
        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(CATEGORIES_URL + categoryId + "/products/");
        System.out.println(resp.asString());
        System.out.println(resp.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL + categoryId + "/products/");
        logger.getStatus();
        if (!resp.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId);
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
            logger.log(Status.INFO, "CATEGORIES ID USED: " + categoryId);
        }
    }
}

//    @Test(testName = "GETTING THE DISTRIBUTED CATALOGUE BY OPERATION ID", description = "Using operationId= random number form 1 to 3856")
//    public void GetGameCatalogueProducts_from_alphaShop_99() throws IOException {
//        String versionId = "20679428";
//        String token1 = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI4NjEwOThhYS1lZDgxLTRhMGYtOTliZS0wNzBiZWI4N2Y4ZDkiLCJpc3MiOiJhdXRoLXNlcnZpY2UiLCJpYXQiOjE1MzUzNjg1NDgsImV4cCI6MTUzNTM3MjE0OCwic2NvcGVzIjoidXNlciJ9.ZrH6tnWd6ZUkpzY9T59HIKWs_LDQIHsFX-E47yVj9m4";
//        Response resp = given().log().path().header("Authorization", "Bearer " + token1).log().method().when().get("http://alpha-app-backend-tim-gameloftstore.eu-west-1.elasticbeanstalk.com/catalogue/download_link?versionId="+versionId);
//        System.out.println(resp.asString());
//        System.out.println(resp.getStatusCode());
//        logger = extent.createTest("test1");
//        logger.getStatus();
//        if (resp.getStatusLine().contains("200")) {
//            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + versionId);
//            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
//
//        } else  {
//            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + versionId);
//            logger.log(Status.INFO, "BODY: " + resp.getBody().asString());
//        }
//    }

//    //GAMES BY OPERATION ID, PLATFORM ID, LANGUAGE ISO CODE
//    @Test(testName = "GAMES BY OPERATION ID, PLATFORM ID, LANGUAGE ISO CODE", description = "Using operationId=0 , platformId=0 , languageIsoCode=0")
//    public void GetGameCatalogueProducts_from_alphaShop_37() throws IOException {
//        int operationId = 0;
//        int platformId = 0;
//        int languageIsoCode = 0;
//        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        System.out.println(resp.asString());
//        System.out.println(resp.getStatusCode());
//        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        logger.getStatus();
//        if (!resp.getStatusLine().contains("200")) {
//            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
//        } else  {
//            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//        }
//    }
//
//    @Test(testName = "GAMES BY OPERATION ID, PLATFORM ID, LANGUAGE ISO CODE", description = "Using operationId=0 , platformId=0 , languageIsoCode=1(valid value)")
//    public void GetGameCatalogueProducts_from_alphaShop_38() throws IOException {
//        int operationId = 0;
//        int platformId = 0;
//        int languageIsoCode = 1;
//        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        System.out.println(resp.asString());
//        System.out.println(resp.getStatusCode());
//        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        logger.getStatus();
//        if (!resp.getStatusLine().contains("200")) {
//            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
//        } else  {
//            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//        }
//    }
//
//    @Test(testName = "GAMES BY OPERATION ID, PLATFORM ID, LANGUAGE ISO CODE", description = "Using valid platformId, languageIsoCode=1 and invalid operationId")
//    public void GetGameCatalogueProducts_from_alphaShop_39() throws IOException {
//        int operationId = 0;
//        int platformId = 6686;
//        int languageIsoCode = 2;
//        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        System.out.println(resp.asString());
//        System.out.println(resp.getStatusCode());
//        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        logger.getStatus();
//        if (!resp.getStatusLine().contains("200")) {
//            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
//        } else  {
//            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//        }
//    }
//
//    @Test(testName = "GAMES BY OPERATION ID, PLATFORM ID, LANGUAGE ISO CODE", description = "Using valid platformId, operationId and invalid languageIsoCode")
//    public void GetGameCatalogueProducts_from_alphaShop_40() throws IOException {
//        int operationId = 19574;
//        int platformId = 6686;
//        int languageIsoCode = 0;
//        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        System.out.println(resp.asString());
//        System.out.println(resp.getStatusCode());
//        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        logger.getStatus();
//        if (!resp.getStatusLine().contains("200")) {
//            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
//        } else  {
//            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//        }
//    }
//
//    @Test(testName = "GAMES BY OPERATION ID, PLATFORM ID, LANGUAGE ISO CODE", description = "Using valid platformId, operationId and invalid languageIsoCode")
//    public void GetGameCatalogueProducts_from_alphaShop_41() throws IOException {
//        int operationId = 19574;
//        int platformId = 6686;
//        int languageIsoCode = 0;
//        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        System.out.println(resp.asString());
//        System.out.println(resp.getStatusCode());
//        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        logger.getStatus();
//        if (!resp.getStatusLine().contains("200")) {
//            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
//        } else  {
//            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//        }
//    }
//
//    @Test(testName = "GAMES BY OPERATION ID, PLATFORM ID, LANGUAGE ISO CODE", description = "Using valid operationId, platformId and languageIsoCode")
//    public void GetGameCatalogueProducts_from_alphaShop_42() throws IOException {
//        int operationId = 19574;
//        int platformId = 6686;
//        int languageIsoCode = 1;
//        String user_agent = "";
//        Response resp = given().log().path().header("Authorization", "Bearer " + token).log().method().when().get(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        System.out.println(resp.asString());
//        System.out.println(resp.getStatusCode());
//        logger = extent.createTest(GAMES_URL +operationId + "/"+ platformId + "/" + languageIsoCode);
//        logger.getStatus();
//        if (!resp.getStatusLine().contains("200")) {
//            logger.log(Status.PASS, " - RESPONSE RETURNED: " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode);
//        } else  {
//            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            logger.log(Status.INFO, "OPERATION ID USED: " + operationId);
//            logger.log(Status.INFO, "PLATFORM ID USED: " + platformId);
//            logger.log(Status.INFO, "LANGUAGE ISO CODE USED: " + languageIsoCode + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//        }
//    }
