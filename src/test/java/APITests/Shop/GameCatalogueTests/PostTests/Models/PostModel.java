package APITests.Shop.GameCatalogueTests.PostTests.Models;

public class PostModel {
    private String assoc;
    private int assoc_id ;
    private String type;
    private String identifier;
    private String name;
    private String i18n;
    private boolean web_display;
    private boolean web_list;
    private boolean display_wap_home;
    private boolean display_wap_category;
    private int rank;

    public String getAssoc() {
        return assoc;
    }

    public void setAssoc(String assoc) {
        this.assoc = assoc;
    }

    public int getAssoc_id() {
        return assoc_id;
    }

    public void setAssoc_id(int assoc_id) {
        this.assoc_id = assoc_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getI18n() {
        return i18n;
    }

    public void setI18n(String i18n) {
        this.i18n = i18n;
    }

    public boolean isWeb_display() {
        return web_display;
    }

    public void setWeb_display(boolean web_display) {
        this.web_display = web_display;
    }

    public boolean isWeb_list() {
        return web_list;
    }

    public void setWeb_list(boolean web_list) {
        this.web_list = web_list;
    }

    public boolean isDisplay_wap_home() {
        return display_wap_home;
    }

    public void setDisplay_wap_home(boolean display_wap_home) {
        this.display_wap_home = display_wap_home;
    }

    public boolean isDisplay_wap_category() {
        return display_wap_category;
    }

    public void setDisplay_wap_category(boolean display_wap_category) {
        this.display_wap_category = display_wap_category;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public PostModel(String assoc, int assoc_id, String type, String identifier, String name, String i18n, boolean web_display, boolean web_list, boolean display_wap_home, boolean display_wap_category, int rank) {
        this.assoc = assoc;
        this.assoc_id = assoc_id;
        this.type = type;
        this.identifier = identifier;
        this.name = name;
        this.i18n = i18n;
        this.web_display = web_display;
        this.web_list = web_list;
        this.display_wap_home = display_wap_home;
        this.display_wap_category = display_wap_category;
        this.rank = rank;
    }

    public PostModel() {
    }
}
