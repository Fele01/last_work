package APITests.Shop.GameCatalogueTests.PostTests;

import APITests.Shop.GameCatalogueTests.GameCatalogueBaseTest;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


import static com.jayway.restassured.RestAssured.given;

public class GameCataloguePostTests extends GameCatalogueBaseTest {
//    @DataProvider(name = "JSONDataProviderCollection")
//    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
//        ObjectMapper mapper = new ObjectMapper();
//        Collection<Object[]> dp = new ArrayList<>();
//
//        File[] files = getListOfFiles("postTestJsonData");
//        for (File f : files) {
//            PostModel m = mapper.readValue(f, PostModel.class);
//            dp.add(new Object[]{m});
//        }
//        return dp.iterator();
//    }

    @Test(/*dataProvider = "JSONDataProviderCollection", */testName = "POST PRODUCTS", description = "POST A PRODUCT")
    public void PostGameCatalogueProducts_1() throws IOException {
        String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"postTestData_01.json")));
        Response postCategory = given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .when()
                .body(postCategoryFile)
                .log()
                .all()
                .post(CATEGORIES_URL)
                .then()
                .log()
                .all()
                .extract()
                .path("id");
        System.out.println(postCategory.asString());
        System.out.println(postCategory.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL);
        logger.getStatus();
        if (!postCategory.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
        }
    }

    @Test(testName = "POST PRODUCTS", description = "POST A PRODUCT")
    public void PostGameCatalogueProducts_2() throws IOException {
        String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"postTestData_02.json")));
        Response postCategory = given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .when()
                .body(postCategoryFile)
                .log()
                .all()
                .post(CATEGORIES_URL)
                .then()
                .log()
                .all()
                .extract()
                .path("id");
        System.out.println(postCategory.asString());
        System.out.println(postCategory.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL);
        logger.getStatus();
        if (!postCategory.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.toString());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
        }
    }

    @Test(testName = "POST PRODUCTS", description = "POST A PRODUCT")
    public void PostGameCatalogueProducts_3() throws IOException {
        String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"postTestData_03.json")));
        Response postCategory = given()
                .contentType(ContentType.JSON)
                .header("Authorization", "Bearer " + token)
                .when()
                .body(postCategoryFile)
                .log()
                .all()
                .post(CATEGORIES_URL)
                .then()
                .log()
                .all()
                .extract()
                .path("id");
        System.out.println(postCategory.asString());
        System.out.println(postCategory.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL);
        logger.getStatus();
        if (!postCategory.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.toString());
        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
        }
    }

    @Test(testName = "POST PRODUCTS", description = "POST A PRODUCT")
    public void PostGameCatalogueProducts_4() throws IOException {

        //curl -X POST "http://ecomapis.gameloft.org/shop_alpha/catalogue/categories/?assoc=Operation&assoc_id=15699&type=family&identifier=tim_test_1257&name=Tim%20Test%201257&i18n=%5B%22en%22%3A%20%22English%22%2C%20%22fr%22%3A%20%22French%22%5D&web_display=false&web_list=false&display_wap_home=false&display_wap_category=false&rank=5" -H "accept: application/json" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3ZWJ0b29scy10aW0tY21zIiwiaXNzIjoiR0FBIiwiaWF0IjoxNTMxOTA4MzM1fQ.vfTC9qH95XOwwF5iJlh77A6WkMiyiugv1aPhsp_Vm24DYIN0MXVHufB0SVosZD2hM_T5jHit-KZT3AhY_UqxoxD-PbtG0h-z8J5LRbGpqz7GBMi5mFp4Ow8CDF2FUmhZy3-EhBeAWzPqAgmoPsVv-_Nixbr8S4mVzxIZE-h1n1E"
        String postCategoryFile = new String(Files.readAllBytes(Paths.get(CATEGORIES_POST_JSON_PATH+"postTestData_06.json")));
        Response postCategory = given()
                .contentType("application/json")
                .header("Authorization", "Bearer " + token)
                .when()
                .body(postCategoryFile)
                .log()
                .all()
                .post(CATEGORIES_URL)
                .then()
                .log()
                .all()
                .extract()
                .path("id");
        System.out.println(postCategory.asString());
        System.out.println(postCategory.getStatusCode());
        logger = extent.createTest(CATEGORIES_URL);
        logger.getStatus();
        if (!postCategory.getStatusLine().contains("200")) {
            logger.log(Status.PASS, " - RESPONSE RETURNED: " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.toString());

        } else {
            logger.log(Status.FAIL, " - RESPONSE RETURNED:    " + postCategory.getStatusLine());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
            logger.log(Status.INFO, " USED: " + postCategory.asString());
        }
    }
}


