package APITests.Shop.AYCETests.GetTests;

import APITests.Shop.AYCETests.BeforeAndAfterAYCE;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;

public class AYCEGetTests extends BeforeAndAfterAYCE {

    @Test(priority = 0, description = "Get all operations")
    public void retrieveOperation() throws IOException, InterruptedException {
        String clienIdUsed = "2102977619";
        String publicKey = new String(Files.readAllBytes(Paths.get("./public.key")), StandardCharsets.UTF_8);
        logger = extent.createTest("/getAyceClientInfo/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING AYCE CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        String[] uIds = {"EN", "FR", "DE", "IT", "ES", "AR"};
        for(int i = 0; i < uIds.length; i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("client_id", clienIdUsed).param("public_key", publicKey).param("type", "kids").param("lang", uIds[i])
                    .log().path().when().get("https://secure.gameloft.com/subscriptions/api/getAyceClientInfo/");
            if (resp.getStatusLine().indexOf("200") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "STATUS LINE:  " + resp.getStatusLine());
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getBody());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                run("touch response.txt");
                run("echo '" + resp.getBody().asString() + "' >> response.txt");
                run("C:/php/php.exe dec.php");
                String content = new String(Files.readAllBytes(Paths.get("./decResponse.txt")), StandardCharsets.UTF_8);
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "CLIENT ID USED: " + clienIdUsed + "  LANG USED: " + uIds[i]);
                logger.log(Status.INFO, "ENCRYPTED RESPONSE RETURNED:  " + resp.getBody().asString());
                logger.log(Status.INFO, "DECRYPTED RESPONSE RETURNED:  " + content);
                run("rm response.txt");
                run("rm decResponse.txt");
            }
        }
    }

    @Test(priority = 0, description = "Get all operations")
    public void retrieveOperation2() throws IOException, InterruptedException {
        String publicKey = new String(Files.readAllBytes(Paths.get("./public.key")), StandardCharsets.UTF_8);
        logger = extent.createTest("/getAyceClientInfo/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING AYCE CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        ArrayList<String> clientIds = generateNumeric(10, 10);
        String[] uIds = {"EN", "FR", "DE", "IT", "ES", "AR"};
        for(int j = 0; j < clientIds.size(); j++) {
            for (int i = 0; i < uIds.length; i++) {
                Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                        .given().param("client_id", clientIds.get(j)).param("public_key", publicKey).param("type", "kids").param("lang", uIds[i])
                        .log().path().when().get("https://secure.gameloft.com/subscriptions/api/getAyceClientInfo/");
                if (resp.getStatusLine().indexOf("200") == -1) {
                    logger.log(Status.FAIL, "RESPONSE ISSUE");
                    logger.log(Status.INFO, "STATUS LINE:  " + resp.getStatusLine());
                    logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getBody());
                } else if (resp.getStatusLine().indexOf("200") != -1) {
                    run("touch response.txt");
                    run("echo '" + resp.getBody().asString() + "' >> response.txt");
                    run("C:/php/php.exe dec.php");
                    String content = new String(Files.readAllBytes(Paths.get("./decResponse.txt")), StandardCharsets.UTF_8);
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "CLIENT ID USED: " + clientIds.get(j) + "  LANG USED: " + uIds[i]);
                    logger.log(Status.INFO, "ENCRYPTED RESPONSE RETURNED:  " + resp.getBody().asString());
                    logger.log(Status.INFO, "DECRYPTED RESPONSE RETURNED:  " + content);
                    run("rm response.txt");
                    run("rm decResponse.txt");
                }
            }
        }
    }

    @Test(priority = 0, description = "Decrypt Response using invalid client ids")
    public void retrieveOperation3() throws IOException, InterruptedException {
        String publicKey = new String(Files.readAllBytes(Paths.get("./public.key")), StandardCharsets.UTF_8);
        logger = extent.createTest("/getAyceClientInfo/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING AYCE CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        ArrayList<String> clientIds = generatingRandomString(5, 10);
        String[] uIds = {"EN", "FR", "DE", "IT", "ES", "AR"};
        for(int j = 0; j < clientIds.size(); j++) {
            for (int i = 0; i < uIds.length; i++) {
                Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                        .given().param("client_id", clientIds.get(j)).param("public_key", publicKey).param("type", "kids").param("lang", uIds[i])
                        .log().path().when().get("https://secure.gameloft.com/subscriptions/api/getAyceClientInfo/");
                if (resp.getStatusLine().indexOf("200") == -1) {
                    logger.log(Status.FAIL, "RESPONSE ISSUE");
                    logger.log(Status.INFO, "STATUS LINE:  " + resp.getStatusLine());
                    logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getBody());
                } else if (resp.getStatusLine().indexOf("200") != -1) {
                    run("touch response.txt");
                    run("echo '" + resp.getBody().asString() + "' >> response.txt");
                    run("C:/php/php.exe dec.php");
                    String content = new String(Files.readAllBytes(Paths.get("./decResponse.txt")), StandardCharsets.UTF_8);
                    if(content.indexOf("0000-00-00 00:00:00") != -1){
                        logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                        logger.log(Status.INFO, "CLIENT ID USED: " + clientIds.get(j) + "  LANG USED: " + uIds[i]);
                        logger.log(Status.INFO, "ENCRYPTED RESPONSE RETURNED:  " + resp.getBody().asString());
                        logger.log(Status.INFO, "DECRYPTED RESPONSE RETURNED:  " + content);
                        run("rm response.txt");
                        run("rm decResponse.txt");
                    }
                    else{
                        logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                        logger.log(Status.INFO, "CLIENT ID USED: " + clientIds.get(j) + "  LANG USED: " + uIds[i]);
                        logger.log(Status.INFO, "ENCRYPTED RESPONSE RETURNED:  " + resp.getBody().asString());
                        logger.log(Status.INFO, "DECRYPTED RESPONSE RETURNED:  " + content);
                        logger.log(Status.INFO, "INVALID CLIENT IDS SHOULD NOT HAVE A VALID EXPIRATION DATE");
                        run("rm response.txt");
                        run("rm decResponse.txt");
                    }
                }
            }
        }
    }
}
