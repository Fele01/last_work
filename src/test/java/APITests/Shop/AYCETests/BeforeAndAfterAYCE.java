package APITests.Shop.AYCETests;

import APITests.Shop.PlatformsInfoTests.Utils.Utils;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import models.SettingAndGettingValues;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class BeforeAndAfterAYCE extends Utils {
    public static final SettingAndGettingValues setBeforeTest = new SettingAndGettingValues();
    public ArrayList<String> randomChosenIds = new ArrayList<String>();
    public ArrayList<String> clientIdsTakenForAYCE = new ArrayList<String>();
    public ArrayList<Map<String,?>> jsonAsArrayList;
    public ExtentTest logger;
    public ExtentTest test;
    public ExtentReports extent = new ExtentReports();
    public String timeStarted = generateCurrentTime();

    @BeforeTest
    public void setup(){
        ExtentHtmlReporter reporter = new ExtentHtmlReporter("./test-output/AYCEAPIs" + timeStarted + ".html");
        reporter.config().setTheme(Theme.DARK);
        extent.attachReporter(reporter);
        extent.setSystemInfo("HostName", "QA Automation");
        extent.setSystemInfo("Environment", "QA");
        setBeforeTest.setToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3ZWJ0b29scy10aW0tY21zIiwiaXNzIjoiR0FBIiwiaWF0IjoxNTMxOTA4MzM1fQ.vfTC9qH95XOwwF5iJlh77A6WkMiyiugv1aPhsp_Vm24DYIN0MXVHufB0SVosZD2hM_T5jHit-KZT3AhY_UqxoxD-PbtG0h-z8J5LRbGpqz7GBMi5mFp4Ow8CDF2FUmhZy3-EhBeAWzPqAgmoPsVv-_Nixbr8S4mVzxIZE-h1n1E");
    }

    @AfterMethod
    public void getResult(ITestResult result){
        if(result.getStatus() == ITestResult.FAILURE){
            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + " Test case FAILED due to below issues", ExtentColor.RED));
            test.fail(result.getThrowable());
        }
    }

    @AfterTest
    public void endReport() throws IOException {
        extent.flush();
        putTagForReport("AYCEAPIs" + timeStarted + ".html");
    }
}
