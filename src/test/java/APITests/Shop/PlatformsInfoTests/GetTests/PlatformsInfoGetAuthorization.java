package APITests.Shop.PlatformsInfoTests.GetTests;

import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;

public class PlatformsInfoGetAuthorization extends BeforeAndAfterPlatform {

    @Test(groups = {"noToken"}, priority = 0, description = "Trigger http code 400 when passing no token")
    public void platformsNoAuthorizationShopAlpha() {
        logger = extent.createTest("/platforms/null");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp =given().log().path().log().method()
                .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/");
        if(resp.getStatusLine().indexOf("200") != -1){
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should not be http code 200");
        }
        else if(resp.getStatusLine().indexOf("400") != -1){
            logger.log(Status.PASS, resp.getStatusLine());
            logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
        }
        else{
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
        }
    }

    @Test(groups = {"wrongToken"}, priority = 0, description = "Trigger http code 400 when passing no token")
    public void platformsNoAuthorizationShopAlpha2() {
        logger = extent.createTest("/platforms/null");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp =given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/");
        if(resp.getStatusLine().indexOf("200") != -1){
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should not be http code 200");
        }
        else if(resp.getStatusLine().indexOf("590") != -1){
            logger.log(Status.PASS, resp.getStatusLine());
            logger.log(Status.INFO, "Authorization server error Invalid signature");
        }
        else{
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
        }
    }

    @Test(groups = {"noToken"}, priority = 0, description = "Trigger http code 400 when passing no token")
    public void platformsNoAuthorization() {
        logger = extent.createTest("/platforms/null");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp =given().log().path().log().method()
                .log().path().when().get("https://ecomapis.gameloft.org/platforms/");
        if(resp.getStatusLine().indexOf("200") != -1){
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should not be http code 200");
        }
        else if(resp.getStatusLine().indexOf("400") != -1){
            logger.log(Status.PASS, resp.getStatusLine());
            logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
        }
        else{
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
        }
    }

    @Test(groups = {"wrongToken"}, priority = 0, description = "Trigger http code 400 when passing no token")
    public void platformsNoAuthorization2() {
        logger = extent.createTest("/platforms/null");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp =given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                .log().path().when().get("https://ecomapis.gameloft.org/platforms/");
        if(resp.getStatusLine().indexOf("200") != -1){
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should not be http code 200");
        }
        else if(resp.getStatusLine().indexOf("590") != -1){
            logger.log(Status.PASS, resp.getStatusLine());
            logger.log(Status.INFO, "Authorization server error Invalid signature");
        }
        else{
            logger.log(Status.FAIL, resp.getStatusLine());
            logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
        }
    }

    @Test(groups = {"noToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorization() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("GET gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorization2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("GET gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"noToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationPost() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("POST gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().post("https://ecomapis.gameloft.org/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationPost2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("POST gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().post("https://ecomapis.gameloft.org/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"noToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationPut() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("PUT gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().put("https://ecomapis.gameloft.org/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationPut2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("PUT gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().put("https://ecomapis.gameloft.org/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"noToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationShop() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("GET gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/Shop_Alpha/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationShop2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("GET gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/Shop_Alpha/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"noToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationShopPost() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("POST gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().post("https://ecomapis.gameloft.org/Shop_Alpha/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationShopPost2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("POST gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().post("https://ecomapis.gameloft.org/Shop_Alpha/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"noToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationShopPut() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("PUT gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().put("https://ecomapis.gameloft.org/Shop_Alpha/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 3, description = "Make request with invalid user agents")
    public void platformIdWithoutAuthorizationShopPut2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("PUT gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().put("https://ecomapis.gameloft.org/Shop_Alpha/platforms/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"noToken"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalidIdsGamesGet() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        logger = extent.createTest("GET shop_alpha/platforms/{invalidPlatformId}/games/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert true as condition is TRUE");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 4, description = "POST request with wrong token for API that supports only Get type request")
    public void platformsInvalidIdsGamePost() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        logger = extent.createTest("POST shop_alpha/platforms/{invalidPlatformId}/games/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert true as condition is TRUE");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .log().path().when().post("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"noToken"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalidIdsGamesPut() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        logger = extent.createTest("PUT shop_alpha/platforms/{invalidPlatformId}/games/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert true as condition is TRUE");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().log().method()
                    .log().path().when().put("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("400") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Unexpected credentials type error no Token provided to the header");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }

    @Test(groups = {"wrongToken"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalidIdsGamesPut2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        logger = extent.createTest("PUT shop_alpha/platforms/{invalidPlatformId}/games/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert true as condition is TRUE");
        for(int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken() + "something").log().method()
                    .log().path().when().put("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
            if(resp.getStatusLine().indexOf("200") != -1){
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should not be http code 200");
            }
            else if(resp.getStatusLine().indexOf("590") != -1){
                logger.log(Status.PASS, resp.getStatusLine());
                logger.log(Status.INFO, "Authorization server error Invalid signature");
            }
            else{
                logger.log(Status.FAIL, resp.getStatusLine());
                logger.log(Status.INFO, "should be Unexpected credentials type error HTTP code 400");
            }
        }
    }
}