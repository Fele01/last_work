package APITests.Shop.PlatformsInfoTests.GetTests;

import APITests.Shop.PlatformsInfoTests.Utils.MyResult;
import APITests.Shop.PlatformsInfoTests.Utils.Utils;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.path.json.JsonPath.from;

public class PlatformsInfoGetTests extends BeforeAndAfterPlatform {

    @Test(groups = {"userAgents", "platformIds"}, priority = 0, description = "Return all the JSONs for all platforms")
    public void retrieveAllAvailableIdsAndUserAgents() {
        int howManyIds = 12, howManyUserAgents = 4;
        logger = extent.createTest("/platforms/null");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/");
        String jsonAsString = resp.asString();   //take the response as string
        jsonAsArrayList = from(jsonAsString).get("");
        randomChosenIds = getRandomValues(jsonAsArrayList, howManyIds);
        randomUserAgents = getRandomUserAgents(jsonAsArrayList, howManyUserAgents);
        logger.log(Status.INFO, resp.getStatusLine());
        logger.log(Status.INFO, "THE REQUEST RETURNED " + randomChosenIds.size() + " IDS THAT MUST BE USED AS VALUES FOR THE 'platformId' variable");
    }

    @Test(groups = {"userAgents"}, priority = 1, description = "Check the response for a random number of user agents")
    public void platformUserAgent() throws IOException, Exception {
        String decodedParam;
        logger = extent.createTest("shop_alpha/platforms/?user_agent=");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUserAgents.size(); i++) {
            decodedParam = URLEncoder.encode(randomUserAgents.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUserAgents.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/");
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Request method not allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUserAgents.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                String responseToCheck = resp.asString().replace("null", "\"null\"");
                MyResult result = Utils.checkResponse(responseToCheck, "./src/test/java/APITests/Shop/PlatformsInfoTests/PlatformsInfoMoldes/platform.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "USER AGENT USED: " + randomUserAgents.get(i) + " - RESPONSE AND MODEL ARE SIMILAR");
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomUserAgents.get(i));
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            } else {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUserAgents.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    //C:\Users\gabriel.mutis\IdeaProjects\pullTuesDay\src\test\java\APITests\Shop\PlatformsInfoTests\PlatformsInfoMoldes\platform.json


    @Test(groups = {"userAgents"}, priority = 1, description = "Check the response for a random number of user agents")
    public void platformUserAgent3() throws Exception {
        logger = extent.createTest("shop_alpha/platforms/?user_agent=BTPC-901");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                .given().param("user_agent", "BTPC-901")
                .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/");
        if (resp.getStatusLine().indexOf("405") != -1) {
            logger.log(Status.PASS, "Request method not allowed");
            logger.log(Status.INFO, "PLATFORM ID USED: " + "BTPC-901" + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        } else if (resp.getStatusLine().indexOf("200") != -1) {
            String responseToCheck = resp.asString().replace("null", "\"null\"");
            MyResult result = Utils.checkResponse(responseToCheck, "./src/test/java/APITests/Shop/PlatformsInfoTests/PlatformsInfoMoldes/platform.json");
            if (result == null) {
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "USER AGENT USED: " + "BTPC-901" + " - RESPONSE AND MODEL ARE SIMILAR");
            } else {
                logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + "BTPC-901");
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        } else {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "PLATFORM ID USED: " + "BTPC-901" + " - RESPONSE RETURNED:    " + resp.getStatusLine());
        }
    }

    @Test(groups = {"userAgents"}, priority = 1, description = "Check the response for a random number of user agents")
    public void platformUserAgent2() throws IOException {
        String decodedParam;
        logger = extent.createTest("gameloft.org/platforms/?user_agent=");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUserAgents.size(); i++) {
            decodedParam = URLEncoder.encode(randomUserAgents.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUserAgents.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/");
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Request method not allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUserAgents.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                String responseToCheck = resp.asString().replace("null", "\"null\"");
                MyResult result = Utils.checkResponse(responseToCheck, "./src/test/java/APITests/Shop/PlatformsInfoTests/PlatformsInfoMoldes/platform.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "USER AGENT USED: " + randomUserAgents.get(i) + " - RESPONSE AND MODEL ARE SIMILAR");
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomUserAgents.get(i));
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            } else {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUserAgents.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"platformIds"}, priority = 2, description = "Check the response for a random number of platform ids")
    public void platforms() throws IOException {
        logger = extent.createTest("shop_alpha/platforms/{platformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomChosenIds.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomChosenIds.get(i));
            if (resp.getStatusLine().indexOf("200") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomChosenIds.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                String responseToCheck = resp.asString().replace("null", "\"null\"");
                MyResult result = Utils.checkResponse(responseToCheck, "./src/test/java/APITests/Shop/PlatformsInfoTests/PlatformsInfoMoldes/platform.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "USER AGENT USED: " + randomChosenIds.get(i) + " - RESPONSE AND MODEL ARE SIMILAR");
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomChosenIds.get(i));
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getSecond().toString());
                }
            }
        }
    }

    @Test(groups = {"platformIds"}, priority = 2, description = "Check the response for a random number of platform ids")
    public void platforms2() throws IOException {
        logger = extent.createTest("gameloft.org/platforms/{platformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomChosenIds.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/" + randomChosenIds.get(i));
            if (resp.getStatusLine().indexOf("200") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomChosenIds.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                String responseToCheck = resp.asString().replace("null", "\"null\"");
                MyResult result = Utils.checkResponse(responseToCheck, "./src/test/java/APITests/Shop/PlatformsInfoTests/PlatformsInfoMoldes/platform.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "USER AGENT USED: " + randomChosenIds.get(i) + " - RESPONSE AND MODEL ARE SIMILAR");
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomChosenIds.get(i));
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getSecond().toString());
                }
            }
        }
    }

//    @Test(priority = 2, description = "Check the response for a random number of platform ids")
//    public void platformsGames() throws IOException {
//        logger = extent.createTest("shop_alpha/platforms/{platformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert pass as condition is True");
//        for(int i = 0; i < randomChosenIds.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomChosenIds.get(i) + "/games/");
//            if(resp.getStatusLine().indexOf("200") == -1){
//                logger.log(Status.FAIL, "RESPONSE ISSUE");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomChosenIds.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/PlatformsInfoTests/PlatformsInfoMoldes/games.json");
//                if(result == null){
//                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
//                    logger.log(Status.INFO, "USER AGENT USED: " + randomChosenIds.get(i) + " - RESPONSE AND MODEL ARE SIMILAR");
//                }
//                else{
//                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
//                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomChosenIds.get(i));
//                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getFirst().toString());
//                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getSecond().toString());
//                }
//            }
//        }
//    }
//
//    @Test(priority = 2, description = "Check the response for a random number of platform ids")
//    public void platformsGames2() throws IOException{
//        logger = extent.createTest("gameloft.org/platforms/{platformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert pass as condition is True");
//        for(int i = 0; i < randomChosenIds.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/" + randomChosenIds.get(i) + "/games/");
//            if(resp.getStatusLine().indexOf("200") == -1){
//                logger.log(Status.FAIL, "RESPONSE ISSUE");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomChosenIds.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/PlatformsInfoTests/PlatformsInfoMoldes/games.json");
//                if(result == null){
//                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
//                    logger.log(Status.INFO, "USER AGENT USED: " + randomChosenIds.get(i) + " - RESPONSE AND MODEL ARE SIMILAR");
//                }
//                else{
//                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
//                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomChosenIds.get(i));
//                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getFirst().toString());
//                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getSecond().toString());
//                }
//            }
//        }
//    }

    @Test(groups = {"userAgents", "invalidUserAgents"}, priority = 3, description = "Make request with invalid user agents")
    public void platformUserAgentInvalid() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("shop_alpha/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/");
            if (resp.getStatusLine().indexOf("404") != -1) {
                logger.log(Status.PASS, "Should return 404 Not found");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 404");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"userAgents", "invalidUserAgents"}, priority = 3, description = "Make request with invalid user agents")
    public void platformUserAgentInvalid2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("GET gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        String responseStatusLine;
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/");
            responseStatusLine = resp.getStatusLine();
            if (responseStatusLine.indexOf("404") != -1) {
                logger.log(Status.PASS, "Should return 404 Not found");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + responseStatusLine);
            } else if (responseStatusLine.indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + responseStatusLine);
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 404");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + responseStatusLine);
            }
        }
    }

    @Test(groups = {"userAgents", "invalidUserAgents", "badrequests"}, priority = 3, description = "Make request with invalid user agents")
    public void platformUserAgentInvalidPost() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("POST shop_alpha/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().post("https://ecomapis.gameloft.org/shop_alpha/platforms/");
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Method Not Allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: POST | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"userAgents", "invalidUserAgents", "badrequests"}, priority = 3, description = "Make request with invalid user agents")
    public void platformUserAgentInvalidPut() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("PUT shop_alpha/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().put("https://ecomapis.gameloft.org/shop_alpha/platforms/");
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Method Not Allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: PUT | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"userAgents", "invalidUserAgents", "badrequests"}, priority = 3, description = "Make request with invalid user agents")
    public void platformUserAgentInvalidPost2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("POST gameloft.org/platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().post("https://ecomapis.gameloft.org/platforms/");
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Method Not Allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: POST | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"userAgents", "invalidUserAgents", "badrequests"}, priority = 3, description = "Make request with invalid user agents")
    public void platformUserAgentInvalidPut2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        String decodedParam;
        logger = extent.createTest("PUT /platforms/?user_agent=invalid");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            decodedParam = URLEncoder.encode(randomUsersGenerated.get(i), "UTF-8");
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .given().param("user_agent", randomUsersGenerated.get(i))
                    .log().path().when().put("https://ecomapis.gameloft.org/platforms/");
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Method Not Allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: PUT | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"platformIds", "invalidUserAgents"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalid() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        logger = extent.createTest("GET shop_alpha/platforms/{invalidPlatformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("404") != -1) {
                logger.log(Status.PASS, "Should return 404 Not found");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 404");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"platformIds", "invalidPlatformIds", "badrequests"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalidPost() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        logger = extent.createTest("POST shop_alpha/platforms/{invalidPlatformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().post("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Should return 405 Request method not allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: POST | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"platformIds", "invalidPlatformIds", "badrequests"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalidPut() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        logger = extent.createTest("PUT shop_alpha/platforms/{invalidPlatformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().put("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Should return 405 Request method not allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: PUT | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"platformIds", "invalidPlatformIds"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalid2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        logger = extent.createTest("GET gameloft.org/platforms/{invalidPlatformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert true as condition is TRUE");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/" + randomUsersGenerated.get(i));
            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
            if (resp.getStatusLine().indexOf("404") != -1) {
                logger.log(Status.PASS, "Should return 404 Not found");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 404");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"platformIds", "invalidPlatformIds", "badrequests"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalid2Post() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        logger = extent.createTest("POST gameloft.org/platforms/{invalidPlatformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert true as condition is TRUE");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().post("https://ecomapis.gameloft.org/platforms/" + randomUsersGenerated.get(i));
            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Should return 405 Request method not allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: POST | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

    @Test(groups = {"platformIds", "invalidPlatformIds", "badrequests"}, priority = 4, description = "Check the response for a random invalid number of platform ids")
    public void platformsInvalid2Put() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);    //generates 4 invalid user_agets
        logger = extent.createTest("PUT gameloft.org/platforms/{invalidPlatformId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING PLATFORM CATEGORY");
        logger.log(Status.PASS, "Assert true as condition is TRUE");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().put("https://ecomapis.gameloft.org/platforms/" + randomUsersGenerated.get(i));
            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
            if (resp.getStatusLine().indexOf("405") != -1) {
                logger.log(Status.PASS, "Should return 405 Request method not allowed");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            } else {
                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
                logger.log(Status.INFO, "REQUEST TYPE USED: POST | AND PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
            }
        }
    }

//    @Test(priority = 4, description = "Check the response for a random invalid number of platform ids")
//    public void platformsInvalidIdsGames() throws IOException {
//        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
//        logger = extent.createTest("GET shop_alpha/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert true as condition is TRUE");
//        for(int i = 0; i < randomUsersGenerated.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
//            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
//            if(resp.getStatusLine().indexOf("404") != -1){
//                logger.log(Status.PASS, "Should return 404 Not found");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else{
//                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 404");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//        }
//    }
//
//    @Test(priority = 4, description = "Check the response for a random invalid number of platform ids")
//    public void platformsInvalidIdsGamesPost() throws IOException {
//        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
//        logger = extent.createTest("POST shop_alpha/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert true as condition is TRUE");
//        for(int i = 0; i < randomUsersGenerated.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().post("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
//            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
//            if(resp.getStatusLine().indexOf("405") != -1){
//                logger.log(Status.PASS, "Should return 405 Request method not allowed");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else{
//                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//        }
//    }
//
//    @Test(priority = 4, description = "Check the response for a random invalid number of platform ids")
//    public void platformsInvalidIdsGamesPut() throws IOException {
//        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
//        logger = extent.createTest("PUT shop_alpha/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert true as condition is TRUE");
//        for(int i = 0; i < randomUsersGenerated.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().put("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
//            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
//            if(resp.getStatusLine().indexOf("405") != -1){
//                logger.log(Status.PASS, "Should return 405 Request method not allowed");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else{
//                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//        }
//    }
//
//    @Test(priority = 4, description = "Check the response for a random invalid number of platform ids")
//    public void platformsInvalidIdsGames2() throws IOException {
//        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
//        logger = extent.createTest("GET gameloft.org/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert true as condition is TRUE");
//        for(int i = 0; i < randomUsersGenerated.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/" + randomUsersGenerated.get(i) + "/games/");
//            if(resp.getStatusLine().indexOf("404") != -1){
//                logger.log(Status.PASS, "Should return 404 Not found");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else{
//                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 404");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//        }
//    }
//
//    @Test(priority = 4, description = "Check the response for a random invalid number of platform ids")
//    public void platformsInvalidIdsGamesPost2() throws IOException {
//        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
//        logger = extent.createTest("POST gameloft.org/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert true as condition is TRUE");
//        for(int i = 0; i < randomUsersGenerated.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().post("https://ecomapis.gameloft.org/platforms/" + randomUsersGenerated.get(i) + "/games/");
//            if(resp.getStatusLine().indexOf("405") != -1){
//                logger.log(Status.PASS, "Should return 405 Request method not allowed");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else{
//                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//        }
//    }
//
//    @Test(priority = 4, description = "Check the response for a random invalid number of platform ids")
//    public void platformsInvalidIdsGamesPut2() throws IOException {
//        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
//        logger = extent.createTest("PUT gameloft.org/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.PASS, "Assert true as condition is TRUE");
//        for(int i = 0; i < randomUsersGenerated.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().put("https://ecomapis.gameloft.org/platforms/" + randomUsersGenerated.get(i) + "/games/");
//            if(resp.getStatusLine().indexOf("405") != -1){
//                logger.log(Status.PASS, "Should return 405 Request method not allowed");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else if(resp.getStatusLine().indexOf("200") != -1){
//                logger.log(Status.FAIL, "SHOULD NOT RETURN HTTP CODE 200");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//            else{
//                logger.log(Status.FAIL, "SHOULD RETURN HTTP CODE 405");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + randomUsersGenerated.get(i) + " - RESPONSE RETURNED:    " + resp.getStatusLine());
//            }
//        }
//    }

//    @Test(priority = 4, description = "Check the response for a random invalid number of platform ids")
//    public void testLength() throws IOException {
//        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 100);    //generates 4 invalid user_agets
//        ArrayList<String> statusResponse = new ArrayList<String>();
//        ArrayList<String> platformIdsUsed = new ArrayList<String>();
//        for(int i = 0; i < randomUsersGenerated.size(); i++) {
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomUsersGenerated.get(i) + "/games/");
//            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
//            if(resp.getStatusLine().indexOf("200") == -1){
//                statusResponse.add(resp.getStatusLine());
//                platformIdsUsed.add(randomUsersGenerated.get(i));
//            }
//        }
//        logger = extent.createTest("shop_alpha/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.FAIL, "Assert fail as condition is FALSE");
//        if(statusResponse.size() > 0){
//            for(int j = 0; j < statusResponse.size(); j++){
//                logger.log(Status.FAIL, "RESPONSE ISSUE");
//                logger.log(Status.INFO, "PLATFORM ID USED: " + platformIdsUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
//            }
//        }
//        else{
//            logger.log(Status.PASS, "RESPONSE OK");
//            logger.log(Status.INFO, "COMPARING RESPONSE WITH MODEL");
//        }
//    }
//
//    @Test(priority = 0, description = "Check the response if between [18990 - 28343]")
//    public void platformsIdsGames() throws IOException {
//        int min = 18990, max = 28343, randomNum;
//        ArrayList<String> statusResponse = new ArrayList<String>();
//        ArrayList<String> platformIdsUsed = new ArrayList<String>();
//        for(int i = 0; i < 10; i++) {
//            randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().get("https://ecomapis.gameloft.org/platforms/" + randomNum + "/games/");
//            System.out.println(resp.getStatusLine());
//            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
//            if(resp.getStatusLine().indexOf("200") == -1){
//                statusResponse.add(resp.getStatusLine());
//                platformIdsUsed.add(Integer.toString(randomNum));
//            }
//        }
//        logger = extent.createTest("gameloft.org/platforms/{invalidPlatformId}/games/");
//        logger.getStatus();
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.log(Status.FAIL, "Assert fail as condition is FALSE");
//        if(statusResponse.size() > 0){
//            for(int j = 0; j < statusResponse.size(); j++){
//                logger.log(Status.INFO, "PLATFORM ID USED: " + platformIdsUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
//            }
//        }
//        else{
//            logger.log(Status.PASS, "RESPONSE OK");
//            logger.log(Status.INFO, "COMPARING RESPONSE WITH MODEL");
//        }
//    }
//
//    @Test(priority = 0, description = "Check the response if between [18990 - 28343] shop_alpha")
//    public void platformsIdsGames2() throws IOException {
//        int min = 18990, max = 28343, randomNum;
//        ArrayList<String> statusResponse = new ArrayList<String>();
//        ArrayList<String> platformIdsUsed = new ArrayList<String>();
//        for(int i = 0; i < 10; i++) {
//            randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
//            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
//                    .log().path().when().get("https://ecomapis.gameloft.org/shop_alpha/platforms/" + randomNum + "/games/");
//            //!Arrays.asList(toCompareResponse).contains(resp.getStatusLine());
//            System.out.println(resp.getStatusLine());
//            if(resp.getStatusLine().indexOf("200") == -1){
//                statusResponse.add(resp.getStatusLine());
//                platformIdsUsed.add(Integer.toString(randomNum));
//            }
//        }
//        logger = extent.createTest("gameloft.org/platforms/{invalidPlatformId}/games/");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING PLATFORM CATEGORY");
//        logger.getStatus();
//        logger.log(Status.FAIL, "Assert fail as condition is FALSE");
//        if(statusResponse.size() > 0){
//            for(int j = 0; j < statusResponse.size(); j++){
//                logger.log(Status.INFO, "PLATFORM ID USED: " + platformIdsUsed.get(j) + " - RESPONSE RETURNED:    " + statusResponse.get(j));
//            }
//        }
//        else{
//            logger.log(Status.PASS, "RESPONSE OK");
//            logger.log(Status.INFO, "COMPARING RESPONSE WITH MODEL");
//        }
//    }
}