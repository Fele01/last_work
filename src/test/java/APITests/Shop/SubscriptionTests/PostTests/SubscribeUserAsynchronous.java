package APITests.Shop.SubscriptionTests.PostTests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "operationId",
        "packId",
        "callbackUrl",
        "fileFormat"
})
public class SubscribeUserAsynchronous {

    @JsonProperty("operationId")
    public String operationId;
    @JsonProperty("packId")
    public String packId;
    @JsonProperty("callbackUrl")
    public String callbackUrl;
    @JsonProperty("fileFormat")
    public String fileFormat;

    /**
     * No args constructor for use in serialization
     */
    public SubscribeUserAsynchronous() {
    }

    /**
     * @param callbackUrl
     * @param packId
     * @param fileFormat
     * @param operationId
     */
    public SubscribeUserAsynchronous(String operationId, String packId, String callbackUrl, String fileFormat) {
        super();
        this.operationId = operationId;
        this.packId = packId;
        this.callbackUrl = callbackUrl;
        this.fileFormat = fileFormat;
    }

}
