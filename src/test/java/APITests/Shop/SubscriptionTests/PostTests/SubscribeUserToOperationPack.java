package APITests.Shop.SubscriptionTests.PostTests;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "operationId",
        "packId",
        "uId",
        "action",
        "purchaseId",
        "platformId",
        "operatorId",
        "source",
        "opref",
        "billingProvider",
        "bearer",
        "agent"
})
public class SubscribeUserToOperationPack {

    @JsonProperty("operationId")
    public String operationId;
    @JsonProperty("packId")
    public String packId;
    @JsonProperty("uId")
    public String uId;
    @JsonProperty("action")
    public String action;
    @JsonProperty("purchaseId")
    public String purchaseId;
    @JsonProperty("platformId")
    public String platformId;
    @JsonProperty("operatorId")
    public String operatorId;
    @JsonProperty("source")
    public String source;
    @JsonProperty("opref")
    public String opref;
    @JsonProperty("billingProvider")
    public String billingProvider;
    @JsonProperty("bearer")
    public String bearer;
    @JsonProperty("agent")
    public String agent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public SubscribeUserToOperationPack() {
    }

    /**
     * @param bearer
     * @param opref
     * @param source
     * @param operatorId
     * @param billingProvider
     * @param packId
     * @param action
     * @param purchaseId
     * @param uId
     * @param agent
     * @param platformId
     * @param operationId
     */
    public SubscribeUserToOperationPack(String operationId, String packId, String uId, String action, String purchaseId, String platformId, String operatorId, String source, String opref, String billingProvider, String bearer, String agent) {
        super();
        this.operationId = operationId;
        this.packId = packId;
        this.uId = uId;
        this.action = action;
        this.purchaseId = purchaseId;
        this.platformId = platformId;
        this.operatorId = operatorId;
        this.source = source;
        this.opref = opref;
        this.billingProvider = billingProvider;
        this.bearer = bearer;
        this.agent = agent;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}