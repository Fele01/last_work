package APITests.Shop.SubscriptionTests.PostTests;

import APITests.Shop.SubscriptionTests.BeforeAndAfterSubscription;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
//import io.restassured.mapper.ObjectMapper;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;

public class SubscriptionPostTests extends BeforeAndAfterSubscription {

//    @Test(priority = 1, description = "Subscribe a User to the selected Operation-Pack")
//    public void subscribeUserToTheSelectedOperationPack() throws IOException, ParseException {
////        String operationId = "15699", packId = "4513";
////        String[] uIds = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
//        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId}");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
//        logger.log(Status.PASS, "Assert pass as condition is True");
//        SubscribeUserToOperationPack subscribe = new SubscribeUserToOperationPack("15699", "4513", "1", "subscribe", "0", "0", "0", "", "", "0", "0", "");
//        ObjectMapper mapper = new ObjectMapper();
//        String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
//        Response resp =given().log().path().header("Content-Type", "application/json").body(subscribeBody)
//                .log().path().when().post("http://ecomapis.gameloft.org/subscription/operation/" + subscribe.operationId + "/pack/" + subscribe.packId + "/uid/" + subscribe.uId);
//        if (resp.getStatusLine().indexOf("200") == -1) {
//            logger.log(Status.FAIL, "RESPONSE ISSUE");
//            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
//        } else if (resp.getStatusLine().indexOf("200") != -1) {
//            logger.log(Status.PASS, "RESPONSE CODE 200");
//            logger.log(Status.INFO, resp.asString());
////                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/SubscriptionTests/SubscriptionInfoModels/user.json");    //check for additional JSONs
////                if (result == null) {
////                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
////                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + operationId);
////                } else {
////                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
////                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + operationId);
////                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
////                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
////                }
//        }
//    }

    @Test(priority = 1, description = "Subscribe a User to the selected Operation-Pack")
    public void subscribeUserToTheSelectedOperationPack3() throws IOException, ParseException {
        String[] uIds = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        SubscribeUserToOperationPack subscribe;
        for (int i = 0; i < uIds.length; i++) {
            subscribe = new SubscribeUserToOperationPack("15699", "4513", uIds[i], "subscribe", "0", "0", "0", "", "", "0", "0", "");
            //ObjectMapper mapper = new ObjectMapper();
            //String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
            Response resp = given().
                    header("Content-Type", "multipart/form-data").
                    multiPart("action", subscribe.action).
                    multiPart("purchaseId", subscribe.purchaseId).
                    multiPart("platformId", subscribe.platformId).
                    multiPart("operatorId", subscribe.operatorId).
                    multiPart("source", subscribe.source).
                    multiPart("opref", subscribe.opref).
                    multiPart("billingProvider", subscribe.billingProvider).
                    multiPart("bearer", subscribe.bearer).
                    multiPart("agent", subscribe.agent).
                    when().
                    post("http://ecomapis.gameloft.org/subscription/operation/" + subscribe.operationId + "/pack/" + subscribe.packId + "/uid/" + subscribe.uId);
            if (resp.getStatusLine().indexOf("200") == -1 && resp.getStatusLine().indexOf("409") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 200");
                logger.log(Status.INFO, resp.asString());
            } else if (resp.getStatusLine().indexOf("409") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 409");
                logger.log(Status.PASS, "ALREADY SUBSCRIBED - USER: " + subscribe.uId);
                logger.log(Status.INFO, resp.asString());
            }
        }
    }

    @Test(priority = 1, description = "Subscribe a User to the selected Operation-Pack")
    public void subscribeGeneratedUserToTheSelectedOperationPack() throws IOException, ParseException {
        ArrayList<String> uIds = generateNumeric(50, 2);
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId} - random generated users");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        SubscribeUserToOperationPack subscribe;
        for (int i = 0; i < uIds.size(); i++) {
            subscribe = new SubscribeUserToOperationPack("15699", "4513", uIds.get(i), "subscribe", "0", "0", "0", "", "", "0", "0", "");
            //ObjectMapper mapper = new ObjectMapper();
            //String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
            Response resp = given().
                    header("Content-Type", "multipart/form-data").
                    multiPart("operationId", subscribe.operationId).
                    multiPart("packId", subscribe.packId).
                    multiPart("uId", subscribe.uId).
                    multiPart("action", subscribe.action).
                    multiPart("purchaseId", subscribe.purchaseId).
                    multiPart("platformId", subscribe.platformId).
                    multiPart("operatorId", subscribe.operatorId).
                    multiPart("source", subscribe.source).
                    multiPart("opref", subscribe.opref).
                    multiPart("billingProvider", subscribe.billingProvider).
                    multiPart("bearer", subscribe.bearer).
                    multiPart("agent", subscribe.agent).
                    when()
                    .log().path().post("http://ecomapis.gameloft.org/subscription/operation/" + subscribe.operationId + "/pack/" + subscribe.packId + "/uid/" + subscribe.uId);
            if (resp.getStatusLine().indexOf("200") == -1 && resp.getStatusLine().indexOf("409") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 200");
                logger.log(Status.INFO, resp.asString());
            } else if (resp.getStatusLine().indexOf("409") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 409");
                logger.log(Status.INFO, "ALREADY SUBSCRIBED - USER: " + subscribe.uId);
            }
        }


//
    }

    @Test
    public void postRequestuploadCSVfile() throws IOException, InterruptedException {
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/users/ - upload CSV file");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        String urlToPost = "http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/";
        String callBackURL = "http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/";
        String fileNameToPassAsCSV = "toPost", outPutFileWithResponse = "response.txt";
        String scriptCode = "#!/bin/bash\ncurl -X POST \"http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" -H \"accept: application/json\" -H \"Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3ZWJ0b29scy10aW0tY21zIiwiaXNzIjoiR0FBIiwiaWF0IjoxNTMxOTA4MzM1fQ.vfTC9qH95XOwwF5iJlh77A6WkMiyiugv1aPhsp_Vm24DYIN0MXVHufB0SVosZD2hM_T5jHit-KZT3AhY_UqxoxD-PbtG0h-z8J5LRbGpqz7GBMi5mFp4Ow8CDF2FUmhZy3-EhBeAWzPqAgmoPsVv-_Nixbr8S4mVzxIZE-h1n1E\" -H \"Content-Type: multipart/form-data\" -F \"callbackUrl=http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" -F \"file=@toPost.csv;type=application/vnd.ms-excel\" -F \"fileFormat=\"csv\"\" >> response.txt";
        File file = new File("script.sh");
        file.createNewFile();
        Files.write(Paths.get("script.sh"), scriptCode.getBytes(), StandardOpenOption.APPEND);
        run("chmod u+x script.sh");
        run("bash script.sh");
        Thread.sleep(2500);    //for safety reasons...
        String content = new String(Files.readAllBytes(Paths.get("./response.txt")), StandardCharsets.UTF_8);
        if (content.indexOf("200") != -1 || content.indexOf("409") != -1) {
            logger.log(Status.PASS, "RESPONSE CODE WAS THE ONE EXPECTED");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + content);
        } else {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + content);
        }
        run("rm script.sh");
        run("rm response.txt");
    }

//    @Test(priority = 1, description = "Subscribe a User to the selected Operation-Pack")
//    public void subscribeUserToTheSelectedOperationPack2() throws IOException {
////        String operationId = "15699", packId = "4513";
////        String[] uIds = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
//        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId}");
//        Assert.assertTrue(true);
//        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
//        logger.log(Status.PASS, "Assert pass as condition is True");
//        SubscribeUserToOperationPack subscribe = new SubscribeUserToOperationPack("15699", "4513", "1", "subscribe", "0", "0", "0", "", "", "0", "0", "");
//        ObjectMapper mapper = new ObjectMapper();
//        String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
//        System.out.println(subscribeBody);
////        ValidatableResponse resp = RestAssured.given().when().header("Authorization", "Bearer "+ setBeforeTest.getToken()).header("Content-Type", "application/json").body(subscribeBody).post("http://ecomapis.gameloft.org/subscription/operation/" + subscribe.operationId + "/pack/" + subscribe.packId + "/uid/" + subscribe.uId);
////        if (resp.getStatusLine().indexOf("200") == -1) {
////            logger.log(Status.FAIL, "RESPONSE ISSUE");
////            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
////        } else if (resp.getStatusLine().indexOf("200") != -1) {
////            logger.log(Status.PASS, "RESPONSE CODE 200");
////            logger.log(Status.INFO, resp.asString());
//        //}
//    }

}
