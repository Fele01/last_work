package APITests.Shop.SubscriptionTests.RequestWirhCallbackURL;

import APITests.Shop.SubscriptionTests.BeforeAndAfterSubscription;
import com.aventstack.extentreports.Status;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ReqCallbackURL extends BeforeAndAfterSubscription {

    @Test
    public void postRequestuploadCSVfile() throws IOException, InterruptedException {
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/users/ - upload CSV file for POST");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
//        String urlToPost = "http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/";
//        String callBackURL = "http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/";
//        String fileNameToPassAsCSV = "toPost", outPutFileWithResponse = "response.txt";
        String scriptCode = "#!/bin/bash\ncurl -X POST \"http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" -H \"accept: application/json\" -H \"Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3ZWJ0b29scy10aW0tY21zIiwiaXNzIjoiR0FBIiwiaWF0IjoxNTMxOTA4MzM1fQ.vfTC9qH95XOwwF5iJlh77A6WkMiyiugv1aPhsp_Vm24DYIN0MXVHufB0SVosZD2hM_T5jHit-KZT3AhY_UqxoxD-PbtG0h-z8J5LRbGpqz7GBMi5mFp4Ow8CDF2FUmhZy3-EhBeAWzPqAgmoPsVv-_Nixbr8S4mVzxIZE-h1n1E\" -H \"Content-Type: multipart/form-data\" -F \"callbackUrl=http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" -F \"file=@toPost.csv;type=application/vnd.ms-excel\" -F \"fileFormat=\"csv\"\" >> response.txt";
        File file = new File("script.sh");
        file.createNewFile();
        Files.write(Paths.get("script.sh"), scriptCode.getBytes(), StandardOpenOption.APPEND);
        run("chmod u+x script.sh");
        run("bash script.sh");
        Thread.sleep(2500);    //for safety reasons...
        String content = new String(Files.readAllBytes(Paths.get("./response.txt")), StandardCharsets.UTF_8);
        if (content.indexOf("200") != -1 || content.indexOf("409") != -1) {
            logger.log(Status.PASS, "RESPONSE CODE WAS THE ONE EXPECTED");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + content);
        } else {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + content);
        }
        run("rm script.sh");
        run("rm response.txt");
    }

    @Test
    public void putRequestuploadCSVfile() throws IOException, InterruptedException {
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/users/ - upload CSV file for PUT");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
//        String urlToPost = "http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/";
//        String callBackURL = "http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/";
//        String fileNameToPassAsCSV = "toPost", outPutFileWithResponse = "response.txt";
        //String scriptCode = "#!/bin/bash\ncurl -X POST \"http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" -H \"accept: application/json\" -H \"Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3ZWJ0b29scy10aW0tY21zIiwiaXNzIjoiR0FBIiwiaWF0IjoxNTMxOTA4MzM1fQ.vfTC9qH95XOwwF5iJlh77A6WkMiyiugv1aPhsp_Vm24DYIN0MXVHufB0SVosZD2hM_T5jHit-KZT3AhY_UqxoxD-PbtG0h-z8J5LRbGpqz7GBMi5mFp4Ow8CDF2FUmhZy3-EhBeAWzPqAgmoPsVv-_Nixbr8S4mVzxIZE-h1n1E\" -H \"Content-Type: multipart/form-data\" -F \"callbackUrl=http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" -F \"file=@toPost.csv;type=application/vnd.ms-excel\" -F \"fileFormat=\"csv\"\" >> response.txt";
        String scriptCode = "#!/bin/bash\ncurl -X PUT -v -H \"accept: application/json\" -H \"Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJ3ZWJ0b29scy10aW0tY21zIiwiaXNzIjoiR0FBIiwiaWF0IjoxNTMxOTA4MzM1fQ.vfTC9qH95XOwwF5iJlh77A6WkMiyiugv1aPhsp_Vm24DYIN0MXVHufB0SVosZD2hM_T5jHit-KZT3AhY_UqxoxD-PbtG0h-z8J5LRbGpqz7GBMi5mFp4Ow8CDF2FUmhZy3-EhBeAWzPqAgmoPsVv-_Nixbr8S4mVzxIZE-h1n1E\" -H \"Content-Type: multipart/form-data;\" -F \"operationId=15699\" -F \"packId=4513\" -F \"action=unsubscribe\" -F \"callbackUrl=http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" -F \"file=@toPut.csv;type=application/vnd.ms-excel\" -F \"fileFormat=\"csv\"\" \"http://ecomapis.gameloft.org/subscription/operation/15699/pack/4513/users/\" >> response.txt";
        File file = new File("script.sh");
        file.createNewFile();
        Files.write(Paths.get("script.sh"), scriptCode.getBytes(), StandardOpenOption.APPEND);
        run("chmod u+x script.sh");
        run("bash script.sh");
        Thread.sleep(2500);    //for safety reasons...
        String content = new String(Files.readAllBytes(Paths.get("./response.txt")), StandardCharsets.UTF_8);
        if (content.indexOf("200") != -1 || content.indexOf("409") != -1) {
            logger.log(Status.PASS, "RESPONSE CODE WAS THE ONE EXPECTED");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + content);
        } else {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + content);
        }
        run("rm script.sh");
        run("rm response.txt");
    }








}
