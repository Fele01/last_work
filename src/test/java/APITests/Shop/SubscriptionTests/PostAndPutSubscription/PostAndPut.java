package APITests.Shop.SubscriptionTests.PostAndPutSubscription;

import APITests.Shop.SubscriptionTests.PostTests.SubscribeUserToOperationPack;
import APITests.Shop.SubscriptionTests.PutTests.UnsubscribeUserToOperationPack;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.jayway.restassured.RestAssured.given;

public class PostAndPut extends SubscribeUser {

    @Test(priority = 1, description = "Subscribe a User to the selected Operation-Pack")
    public void subscribeUserToTheSelectedOperationPack3() throws IOException, ParseException {
        String[] uIds = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId} Subscribe then unsubscribe");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        SubscribeUserToOperationPack subscribe;
        for (int i = 0; i < uIds.length; i++) {
            subscribe = new SubscribeUserToOperationPack("15699", "4513", uIds[i], "subscribe", "0", "0", "0", "", "", "0", "0", "");
            //ObjectMapper mapper = new ObjectMapper();
            //String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
            Response resp = given().
                    header("Authorization", "Bearer " + setBeforeTest.getToken()).
                    header("Content-Type", "multipart/form-data").
                    multiPart("action", subscribe.action).
                    multiPart("purchaseId", subscribe.purchaseId).
                    multiPart("platformId", subscribe.platformId).
                    multiPart("operatorId", subscribe.operatorId).
                    multiPart("source", subscribe.source).
                    multiPart("opref", subscribe.opref).
                    multiPart("billingProvider", subscribe.billingProvider).
                    multiPart("bearer", subscribe.bearer).
                    multiPart("agent", subscribe.agent).
                    when().
                    post("http://ecomapis.gameloft.org/subscription/operation/" + subscribe.operationId + "/pack/" + subscribe.packId + "/uid/" + subscribe.uId);
            if (resp.getStatusLine().indexOf("200") == -1 && resp.getStatusLine().indexOf("409") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 200");
                logger.log(Status.INFO, resp.asString());
            } else if (resp.getStatusLine().indexOf("409") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 409");
                logger.log(Status.PASS, "ALREADY SUBSCRIBED - USER: " + subscribe.uId);
                logger.log(Status.INFO, resp.asString());
            }
        }
    }

    @Test(priority = 0, description = "UnSubscribe a User to the selected Operation-Pack")
    public void unsubscribeUsers() {
        String[] uIds = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId} - unsubscribe after subscription");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        UnsubscribeUserToOperationPack unSubscribe;
        for(int i = 0; i < uIds.length; i++) {
            unSubscribe = new UnsubscribeUserToOperationPack("15699", "4513", uIds[i], "unsubscribe");
            //ObjectMapper mapper = new ObjectMapper();
            //String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
            Response resp = given().
                    header("Authorization", "Bearer " + setBeforeTest.getToken()).
                    header("Content-Type", "application/x-www-form-urlencoded").
                    formParam("action", unSubscribe.action).
                    when()
                    .log().path().put("http://ecomapis.gameloft.org/subscription/operation/" + unSubscribe.operationId + "/pack/" + unSubscribe.packId + "/uid/" + unSubscribe.uId);
            if (resp.getStatusLine().indexOf("200") == -1 && resp.getStatusLine().indexOf("409") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 200");
                logger.log(Status.INFO, resp.asString());
            } else if (resp.getStatusLine().indexOf("409") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 409");
                logger.log(Status.INFO, "ALREADY UNSUBSCRIBED - USER: " + unSubscribe.uId);
            }
        }
    }

}
