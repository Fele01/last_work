package APITests.Shop.SubscriptionTests.PostAndPutSubscription;

import APITests.Shop.SubscriptionTests.BeforeAndAfterSubscription;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "operationId",
        "packId",
        "uId",
        "action"
})
public class UnsubscribeUser extends BeforeAndAfterSubscription {

    @JsonProperty("operationId")
    public String operationId;
    @JsonProperty("packId")
    public String packId;
    @JsonProperty("uId")
    public String uId;
    @JsonProperty("action")
    public String action;

    /**
     * No args constructor for use in serialization
     */
    public UnsubscribeUser() {
    }

    /**
     * @param packId
     * @param action
     * @param uId
     * @param operationId
     */
    public UnsubscribeUser(String operationId, String packId, String uId, String action) {
        super();
        this.operationId = operationId;
        this.packId = packId;
        this.uId = uId;
        this.action = action;
    }

}