package APITests.Shop.SubscriptionTests.PutTests;

import APITests.Shop.SubscriptionTests.BeforeAndAfterSubscription;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;

import static com.jayway.restassured.RestAssured.given;

public class SubscriptionPutTests extends BeforeAndAfterSubscription {

    @Test(priority = 0, description = "UnSubscribe a User to the selected Operation-Pack")
    public void unsubscribeUsers() {
        String[] uIds = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId} - unsubscribe users");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        UnsubscribeUserToOperationPack unSubscribe;
        for(int i = 0; i < uIds.length; i++) {
            unSubscribe = new UnsubscribeUserToOperationPack("15699", "4513", uIds[i], "unsubscribe");
            //ObjectMapper mapper = new ObjectMapper();
            //String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
            Response resp = given().
                    header("Content-Type", "application/x-www-form-urlencoded").
                    formParam("action", unSubscribe.action).
                    when()
                    .log().path().put("http://ecomapis.gameloft.org/subscription/operation/" + unSubscribe.operationId + "/pack/" + unSubscribe.packId + "/uid/" + unSubscribe.uId);
            if (resp.getStatusLine().indexOf("200") == -1 && resp.getStatusLine().indexOf("409") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 200");
                logger.log(Status.INFO, resp.asString());
            } else if (resp.getStatusLine().indexOf("409") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 409");
                logger.log(Status.INFO, "ALREADY UNSUBSCRIBED - USER: " + unSubscribe.uId);
            }
        }
    }

    @Test(priority = 1, description = "UnSubscribe a User to the selected Operation-Pack")
    public void unsubscribeGeneratedUsers() throws IOException, ParseException {
        ArrayList<String> uIds = generateNumeric(10, 2);
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId} - unsubscribe generated users");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        UnsubscribeUserToOperationPack unSubscribe;
        for(int i = 0; i < uIds.size(); i++) {
            unSubscribe = new UnsubscribeUserToOperationPack("15699", "4513", uIds.get(i), "unsubscribe");
            //ObjectMapper mapper = new ObjectMapper();
            //String subscribeBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(subscribe);
            Response resp = given().
                    header("Content-Type", "application/x-www-form-urlencoded").
                    formParam("action", unSubscribe.action).
                    when()
                    .log().path().put("http://ecomapis.gameloft.org/subscription/operation/" + unSubscribe.operationId + "/pack/" + unSubscribe.packId + "/uid/" + unSubscribe.uId);
            if (resp.getStatusLine().indexOf("200") == -1 && resp.getStatusLine().indexOf("409") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 200");
                logger.log(Status.INFO, resp.asString());
            } else if (resp.getStatusLine().indexOf("409") != -1) {
                logger.log(Status.PASS, "RESPONSE CODE 409");
                logger.log(Status.INFO, "ALREADY UNSUBSCRIBED - USER: " + unSubscribe.uId);
            }
        }
    }

}
