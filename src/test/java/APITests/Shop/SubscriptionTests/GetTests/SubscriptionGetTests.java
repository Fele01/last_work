package APITests.Shop.SubscriptionTests.GetTests;

import APITests.Shop.PlatformsInfoTests.Utils.MyResult;
import APITests.Shop.SubscriptionTests.BeforeAndAfterSubscription;
import com.aventstack.extentreports.Status;
import com.jayway.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import static com.jayway.restassured.RestAssured.given;


public class SubscriptionGetTests extends BeforeAndAfterSubscription {

    //C:\Users\gabriel.mutis\IdeaProjects\pullTuesDay\src\test\java\APITests\Shop\SubscriptionTests\SubscriptionInfoModels\operation.json

    @Test(priority = 0, description = "Get all operations")
    public void retrieveOperation() throws IOException {
        logger = extent.createTest("subscription/operation/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/");
        if (resp.getStatusLine().indexOf("200") == -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine());
        } else if (resp.getStatusLine().indexOf("200") != -1) {
            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/operation.json");
            if (result == null) {
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR");
            } else {
                logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR");
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        }
    }

    @Test(priority = 1, description = "Find Operation by ID")
    public void retrieveOperationByID() throws IOException {
        String operationId = "15699";
        logger = extent.createTest("/operation/{operationId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + operationId);
        if (resp.getStatusLine().indexOf("200") == -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + operationId);
        } else if (resp.getStatusLine().indexOf("200") != -1) {
            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/operation.json");
            if (result == null) {
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + operationId);
            } else {
                logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + operationId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        }
    }

    @Test(priority = 1, description = "Get all Packs")
    public void retrieveAllPacks() throws IOException {
        String operationId = "15699";
        logger = extent.createTest("/operation/{operationId}/pack/");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + operationId + "/pack/");
        if (resp.getStatusLine().indexOf("200") == -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + operationId);
        } else if (resp.getStatusLine().indexOf("200") != -1) {
            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/id.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + operationId);
            } else {
                logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + operationId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        }
    }

    @Test(priority = 1, description = "Find Pack by ID")
    public void retrievePackByID() throws IOException {
        String operationId = "15699", packId = "4513";
        logger = extent.createTest("/operation/{operationId}/pack/{packId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + operationId + "/pack/" + packId);
        if (resp.getStatusLine().indexOf("200") == -1) {
            logger.log(Status.FAIL, "RESPONSE ISSUE");
            logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + operationId);
        } else if (resp.getStatusLine().indexOf("200") != -1) {
            MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/id.json");    //check for additional JSONs
            if (result == null) {
                logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + operationId);
            } else {
                logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + operationId);
                logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
            }
        }
    }

    @Test(priority = 1, description = "Get User Status for an Operation-Pack")
    public void retrieveStatusForAnOperationPack() throws IOException {
        String operationId = "15699", packId = "4513";
        String[] uIds = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/uid/{uId}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < uIds.length; i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + operationId + "/pack/" + packId + "/uid/" + uIds[i]);
            if (resp.getStatusLine().indexOf("200") == -1) {
                logger.log(Status.FAIL, "RESPONSE ISSUE");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + operationId);
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/user.json");    //check for additional JSONs
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + operationId);
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + operationId);
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            }
        }
    }

    @Test(priority = 1, description = "Get Users Status for an Operation-Pack")
    public void retrieveUserStatusForOperationPack() throws IOException {
        String operationId = "15699", packId = "4513";
        String[] status = {"active", "pending", "inactive", "suspended", "canceled"};
        logger = extent.createTest("/operation/{operationId}/pack/{packId}/users/{status}");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for(int i = 0; i < status.length; i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + operationId + "/pack/" + packId + "/users/" + status[i]);
            if (resp.getStatusLine().indexOf("204") != -1) {
                logger.log(Status.PASS, "NO CONTENT");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + operationId + " FOR STATUS: " + status[i]);
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/status.json");    //check for additional JSONs
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + operationId);
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + operationId);
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getSecond().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getFirst().toString());
                }
            }
            else{
                logger.log(Status.FAIL, "HTTP CODE SHOULD BE 200 OR 204");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + operationId + " FOR STATUS: " + status[i]);
            }
        }
    }

    @Test(priority = 4, description = "Find Operation by ID using random strings")
    public void retrieveOperationByID2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);
        logger = extent.createTest("/operation/{operationId} invalid value as *String type");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("204") != -1) {
                logger.log(Status.PASS, "RETURNED HTTP CODE 204 FOR INVALID VALUES");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE SHOULD NOT BE HTTP CODE 200");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            }
            else{
                logger.log(Status.FAIL, "REQUEST MADE WITH INVALID VALUE HTTP CODE 204 EXPECTED");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            }
        }
    }

    @Test(priority = 4, description = "Find Operation by ID using random integers")
    public void retrieveOperationByID3() throws IOException {
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 5);
        logger = extent.createTest("/operation/{operationId} invalid value as *integer type");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + randomUsersGenerated.get(i));
            if (resp.getStatusLine().indexOf("204") != -1) {
                logger.log(Status.PASS, "NO CONTENT");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + " FOR " + randomUsersGenerated.get(i));
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/operation.json");
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + randomUsersGenerated.get(i));
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomUsersGenerated.get(i));
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            }
            else{
                logger.log(Status.FAIL, "HTTP CODE SHOULD BE 204 OR 200");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + " FOR " + randomUsersGenerated.get(i));
            }
        }
    }

    @Test(priority = 4, description = "Get all Packs invalid String value")
    public void retrieveAllPacks2() throws IOException {
        ArrayList<String> randomUsersGenerated = generatingRandomString(4, 5);
        logger = extent.createTest("/operation/{operationId}/pack/  invalid value as *String type");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + randomUsersGenerated.get(i) + "/pack/");
            if (resp.getStatusLine().indexOf("204") != -1) {
                logger.log(Status.PASS, "RETURNED HTTP CODE 204 FOR INVALID VALUES");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                logger.log(Status.FAIL, "RESPONSE SHOULD NOT BE HTTP CODE 200");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            }
            else{
                logger.log(Status.FAIL, "REQUEST MADE WITH INVALID VALUE HTTP CODE 204 EXPECTED");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            }
        }
    }

    @Test(priority = 4, description = "Get all Packs invalid integer value")
    public void retrieveAllPacks3() throws IOException {
        ArrayList<String> randomUsersGenerated = generateNumeric(4, 5);
        logger = extent.createTest("/operation/{operationId}/pack/  invalid value as *integer type");
        Assert.assertTrue(true);
        logger.assignCategory("API TESTING SUBSCRIPTION CATEGORY");
        logger.log(Status.PASS, "Assert pass as condition is True");
        for (int i = 0; i < randomUsersGenerated.size(); i++) {
            Response resp = given().log().path().header("Authorization", "Bearer " + setBeforeTest.getToken()).log().method()
                    .log().path().when().get("http://ecomapis.gameloft.org/subscription/operation/" + randomUsersGenerated.get(i) + "/pack/");
            if (resp.getStatusLine().indexOf("204") != -1) {
                logger.log(Status.PASS, "NO CONTENT");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            } else if (resp.getStatusLine().indexOf("200") != -1) {
                MyResult result = checkResponse(resp.asString(), "./src/test/java/APITests/Shop/SubscriptionTests/SubscriptionInfoModels/id.json");    //check for additional JSONs
                if (result == null) {
                    logger.log(Status.PASS, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE SIMILAR FOR " + randomUsersGenerated.get(i));
                } else {
                    logger.log(Status.FAIL, "HTTP RESPONSE CODE 200");
                    logger.log(Status.INFO, "RESPONSE AND MODEL ARE NOT SIMILAR FOR " + randomUsersGenerated.get(i));
                    logger.log(Status.INFO, "What Response has and the Model does not have: " + result.getFirst().toString());
                    logger.log(Status.INFO, "What Model has and the Response does not have: " + result.getSecond().toString());
                }
            }
            else{
                logger.log(Status.FAIL, "HTTP CODE SHOULD BE 200 OR 204");
                logger.log(Status.INFO, "RESPONSE RETURNED:  " + resp.getStatusLine() + "  FOR " + randomUsersGenerated.get(i));
            }
        }
    }
}
