package Utils;

import APITests.Shop.PlatformsInfoTests.Utils.MyResult;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static APITests.Shop.PlatformsInfoTests.Utils.Utils.compareListsOfKeys;
import static APITests.Shop.PlatformsInfoTests.Utils.Utils.takeKeys;
import static com.jayway.restassured.path.json.JsonPath.from;

public class Utils {

    public static void run(String comand) throws IOException, InterruptedException {
        Runtime p = Runtime.getRuntime();
        Process proc = p.exec(new String[]{"C:\\cygwin\\bin\\bash.exe", "-c", comand}, new String[]{"PATH=/cygdrive/c/cygwin/bin"});
        proc.waitFor();
        BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        while (br.ready())
            System.out.println(br.readLine());
    }

    public static String getRandomString(int length, boolean mixNumbers) {
        String[] charactersPool = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
        String[] numericCharactersPool = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

        if (mixNumbers) {
            charactersPool = ArrayUtils.addAll(charactersPool, numericCharactersPool);
        }

        Random r = new Random();
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < length; i++) {
            result.append(charactersPool[r.nextInt(charactersPool.length - 1)]);
        }

        return result.toString();
    }

    public static int getRandomNumber(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }

    public ArrayList<String> getClientIds(ArrayList<Map<String,?>> jsonAsArrayList){
        ArrayList<String> clientIdsTaken = new ArrayList<String>();
        for(int i = 0; i < jsonAsArrayList.size(); i++){
            if(jsonAsArrayList.get(i).get("client_id").toString().length() > 0){
                clientIdsTaken.add(jsonAsArrayList.get(i).get("client_id").toString());
            }
        }
        System.out.println(clientIdsTaken);
        return clientIdsTaken;
    }

    public ArrayList<String> getRandomValues(ArrayList<Map<String,?>> jsonAsArrayList, int howManyToPass){
        Random rand = new Random();
        int randomIndex;
        ArrayList<String> javaPlatforms = new ArrayList<String>();
        ArrayList<String> androidPlatforms = new ArrayList<String>();
        ArrayList<String> randomIdChosen = new ArrayList<String>();
        for(int i = 0; i < jsonAsArrayList.size(); i++){
            if(jsonAsArrayList.get(i).get("agent").toString().indexOf("AndroidGeneric") == -1 && (jsonAsArrayList.get(i).get("agent").toString().indexOf("Android") != -1 || jsonAsArrayList.get(i).get("phone").toString().indexOf("Android") != -1)){
                androidPlatforms.add(jsonAsArrayList.get(i).get("id").toString());
            }
            else if(jsonAsArrayList.get(i).get("agent").toString().indexOf("AndroidGeneric") == -1 && (jsonAsArrayList.get(i).get("phone").toString().indexOf("Android") == -1 && jsonAsArrayList.get(i).get("agent").toString().indexOf("Android") == -1)){
                javaPlatforms.add(jsonAsArrayList.get(i).get("id").toString());
            }
        }
        for(int j = 0; j < howManyToPass; j++){
            if(j % 2 == 0) {
                randomIndex = rand.nextInt(javaPlatforms.size());
                randomIdChosen.add(javaPlatforms.get(randomIndex));
            }
            else{
                randomIndex = rand.nextInt(androidPlatforms.size());
                randomIdChosen.add(androidPlatforms.get(randomIndex));
            }
        }
        randomIdChosen.add("6686");    //hardcoded cuz it's only one android generic
        System.out.println("DEBUG IDS: " + randomIdChosen);
        return randomIdChosen;
    }

    public ArrayList<String> getRandomUserAgents(ArrayList<Map<String,?>> jsonAsArrayList, int howManyToPass){
        Random rand = new Random();
        int randomIndex;
        ArrayList<String> allUserAgents = new ArrayList<String>();
        ArrayList<String> randomUserAgentChosen = new ArrayList<String>();
        for(int i = 0; i < jsonAsArrayList.size(); i++) {
            if (jsonAsArrayList.get(i).get("agent").toString().length() > 1 && jsonAsArrayList.get(i).get("agent").toString().indexOf("AndroidGeneric") == -1 ) {
                allUserAgents.add(jsonAsArrayList.get(i).get("agent").toString());
            }
        }
        System.out.println("check here");
        for(int j = 0; j < howManyToPass; j++){
                randomIndex = rand.nextInt(allUserAgents.size());
            randomUserAgentChosen.add(allUserAgents.get(randomIndex));
        }
        System.out.println("DEBUG USERS: " + randomUserAgentChosen);
        return randomUserAgentChosen;
    }

    public ArrayList<String> generatingRandomString(int howManyTimes, int lengthForStrings) throws IOException {
        Random rand = new Random();
        int randomIndex;
        String generatedString;
        ArrayList<String> generatedRandomUsers = new ArrayList<String>();
        for(int i = 0; i < howManyTimes; i++){
            randomIndex = rand.nextInt(lengthForStrings) + 1;
            byte[] array = new byte[randomIndex]; // length is bounded by 7
            new Random().nextBytes(array);
            generatedString = new String(array, Charset.forName("Unicode"));
            generatedRandomUsers.add(generatedString.replace("?", ""));
        }
//        final Formatter file = new Formatter("generatedStrings.txt");
//        file.format("%s", generatedRandomUsers.toString());
//        file.close();
        System.out.println(generatedRandomUsers);
        return generatedRandomUsers;
    }

    public void putTagForReport(String pathToTheReport) throws IOException{
        Scanner x = new Scanner(new File("./test-output/" + pathToTheReport));
        String textFromFile = "";
        while(x.hasNext()){
            textFromFile = textFromFile + x.next();
        }
        x.close();
        File file = new File("./test-output/" + pathToTheReport);
        if(textFromFile.indexOf("0</span>test(s)failed") != -1){
            File file2 = new File("./test-output/" + "[PASS]" + pathToTheReport);
            file.renameTo(file2);
        }
        else {
            File file2 = new File("./test-output/" + "[FAIL]" + pathToTheReport);
            file.renameTo(file2);
        }
    }


    public static void compareKeys(Object  objOne, Object  objTwo) throws IOException, Exception, JSONException {
        System.out.println("blabla");
        JSONObject obj1 = (JSONObject) objOne;
        JSONObject obj2 = (JSONObject) objTwo;
        System.out.println("blabla");
        Set keys1 = obj1.keySet(), keys2 = obj2.keySet();
        Iterator a = keys1.iterator(), b = keys2.iterator();
        while(a.hasNext()) {
            String key = (String)a.next();
            System.out.print("key : "+key);
        }
        while(b.hasNext()) {
            String key = (String)b.next();
            System.out.print("key : "+key);
        }
    }

    public static Object jsonsEqual(Object obj1, Object obj2) throws JSONException {

        JSONObject diff = new JSONObject();

        if (!obj1.getClass().equals(obj2.getClass())) {
            return diff;
        }

        if (obj1 instanceof JSONObject && obj2 instanceof JSONObject) {
            JSONObject jsonObj1 = (JSONObject) obj1;

            JSONObject jsonObj2 = (JSONObject) obj2;

            List<String> names = new ArrayList(Arrays.asList(JSONObject.getNames(jsonObj1)));
            List<String> names2 = new ArrayList(Arrays.asList(JSONObject.getNames(jsonObj2)));
            if (!names.containsAll(names2) && names2.removeAll(names)) {
                for (String fieldName : names2) {
                    if(jsonObj1.has(fieldName))
                        diff.put(fieldName, jsonObj1.get(fieldName));
                    else if(jsonObj2.has(fieldName))
                        diff.put(fieldName, jsonObj2.get(fieldName));
                }
                names2 = Arrays.asList(JSONObject.getNames(jsonObj2));
            }

            if (names.containsAll(names2)) {
                for (String fieldName : names) {
                    Object obj1FieldValue = jsonObj1.get(fieldName);
                    Object obj2FieldValue = jsonObj2.get(fieldName);
                    Object obj = jsonsEqual(obj1FieldValue, obj2FieldValue);
                    if (obj != null && !checkObjectIsEmpty(obj))
                        diff.put(fieldName, obj);
                }
            }
            return diff;
        } else if (obj1 instanceof JSONArray && obj2 instanceof JSONArray) {

            JSONArray obj1Array = (JSONArray) obj1;
            JSONArray obj2Array = (JSONArray) obj2;
            if (!obj1Array.toString().equals(obj2Array.toString())) {
                JSONArray diffArray = new JSONArray();
                for (int i = 0; i < obj1Array.length(); i++) {
                    Object obj = null;
                    matchFound: for (int j = 0; j < obj2Array.length(); j++) {
                        obj = jsonsEqual(obj1Array.get(i), obj2Array.get(j));
                        if (obj == null) {
                            break matchFound;
                        }
                    }
                    if (obj != null)
                        diffArray.put(obj);
                }
                if (diffArray.length() > 0)
                    return diffArray;
            }
        } else {
            if (!obj1.equals(obj2)) {
                return obj2;
            }
        }

        return null;
    }

    private static boolean checkObjectIsEmpty(Object obj) {
        if (obj == null)
            return true;
        String objData = obj.toString();
        if (objData.length() == 0)
            return true;
        if (objData.equalsIgnoreCase("{}"))
            return true;
        return false;
    }

    public static String takeAndParseJSON(String pathToFileJSON, Boolean arrayJSON) throws ParseException, Exception{
        JSONParser parser = new JSONParser();
        String json;
        if(arrayJSON){
            org.json.simple.JSONArray data = (org.json.simple.JSONArray) parser.parse(
                    new FileReader(pathToFileJSON));//path to the JSON file.
            json = data.toJSONString();
        }
        else{
            org.json.simple.JSONObject data = (org.json.simple.JSONObject) parser.parse(
                    new FileReader(pathToFileJSON));//path to the JSON file.
            json = data.toJSONString();
        }
        return json;
    }

    public static String generateCurrentTime() {
        String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(Calendar.getInstance().getTime());
        String currentTime = "_" + timeStamp;
        return currentTime;
    }


    public static MyResult checkResponse(String responseJSON, String path) throws IOException {
        ArrayList<String> keysResponse, keysModel;
        String model = new String(Files.readAllBytes(Paths.get(path)), StandardCharsets.UTF_8);
        JSONObject modelJSON = new JSONObject(model);
        if (responseJSON.indexOf("[") == 0) {
            ArrayList<Map<String, ?>> responseAsArrayList = from(responseJSON).get("");
            for (int i = 0; i < responseAsArrayList.size() - 1; i++) {
                JSONObject responseJObjectOne = new JSONObject(responseAsArrayList.get(i));
                JSONObject responseJObjectTwo = new JSONObject(responseAsArrayList.get(i + 1));
                ArrayList<String> keysResponseOne = takeKeys(responseJObjectOne);
                ArrayList<String> keysResponseTwo = takeKeys(responseJObjectTwo);
                MyResult result = compareListsOfKeys(keysResponseOne, keysResponseTwo);
                if (result == null) {
                    continue;
                }
                else{
                    System.out.println(result.getFirst() + " && " + result.getSecond());
                    return new MyResult(result.getFirst(), result.getSecond());
                }
            }
            JSONObject responseJObject = new JSONObject(responseAsArrayList.get(0));
            keysResponse = takeKeys(responseJObject);
            keysModel = takeKeys(modelJSON);
            MyResult result = compareListsOfKeys(keysResponse, keysModel);
            if (result == null) {
                return null;
            } else {
                System.out.println("trying to return");
                return new MyResult(result.getFirst(), result.getSecond());
            }
        } else {
            JSONObject responseJObject = new JSONObject(responseJSON);
            keysResponse = takeKeys(responseJObject);
            keysModel = takeKeys(modelJSON);
            MyResult result = compareListsOfKeys(keysResponse, keysModel);
            if (result == null) {
                return null;
            } else {
                return new MyResult(result.getFirst(), result.getSecond());
            }
        }
    }
}
