package models;

public class SettingAndGettingValues {


    private String specificUrl, token, userAgent;

    public void setURL(String specificUrl){
        this.specificUrl = specificUrl;
    }

    public String getURL(){
        return specificUrl;
    }

    public void setToken(String token){
        this.token = token;
    }

    public String getToken(){
        return token;
    }

    public void setParam(String userAgent){ this.userAgent = userAgent; }

    public String getParam(){
        return userAgent;
    }
}
